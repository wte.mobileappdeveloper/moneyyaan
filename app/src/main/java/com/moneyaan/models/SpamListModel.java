package com.moneyaan.models;

/**
 * Created by ErLalji Yadav on 6/29/2017.
 */

public class SpamListModel {
    private String Package_ID;
    private String Package_Name;
    private String Icon;
    private String type;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPackage_ID() {
        return Package_ID;
    }

    public void setPackage_ID(String package_ID) {
        Package_ID = package_ID;
    }

    public String getPackage_Name() {
        return Package_Name;
    }

    public void setPackage_Name(String package_Name) {
        Package_Name = package_Name;
    }

    public String getIcon() {
        return Icon;
    }

    public void setIcon(String icon) {
        Icon = icon;
    }


}
