package com.moneyaan.models;

/**
 * Created by lalji on 28/2/18.
 */

public class ApprovalPaymentHistoryModel {

    String Total_earned;
    String Total_selfpoints;
    String Total_onemonth;
    String Total_oneyear;

    public String getTotal_earned() {
        return Total_earned;
    }

    public void setTotal_earned(String total_earned) {
        Total_earned = total_earned;
    }

    public String getTotal_selfpoints() {
        return Total_selfpoints;
    }

    public void setTotal_selfpoints(String total_selfpoints) {
        Total_selfpoints = total_selfpoints;
    }

    public String getTotal_onemonth() {
        return Total_onemonth;
    }

    public void setTotal_onemonth(String total_onemonth) {
        Total_onemonth = total_onemonth;
    }

    public String getTotal_oneyear() {
        return Total_oneyear;
    }

    public void setTotal_oneyear(String total_oneyear) {
        Total_oneyear = total_oneyear;
    }

    public String getTotal_lifetime() {
        return Total_lifetime;
    }

    public void setTotal_lifetime(String total_lifetime) {
        Total_lifetime = total_lifetime;
    }

    public String getTotal_referal_income_by_child() {
        return Total_referal_income_by_child;
    }

    public void setTotal_referal_income_by_child(String total_referal_income_by_child) {
        Total_referal_income_by_child = total_referal_income_by_child;
    }

    public String getTotal_rmonth_total_income_by_child() {
        return Total_rmonth_total_income_by_child;
    }

    public void setTotal_rmonth_total_income_by_child(String total_rmonth_total_income_by_child) {
        Total_rmonth_total_income_by_child = total_rmonth_total_income_by_child;
    }

    public String getTotal_rone_year_Total_income_by_child() {
        return Total_rone_year_Total_income_by_child;
    }

    public void setTotal_rone_year_Total_income_by_child(String total_rone_year_Total_income_by_child) {
        Total_rone_year_Total_income_by_child = total_rone_year_Total_income_by_child;
    }

    public String getTotal_rLife_time_Total_income_by_child() {
        return Total_rLife_time_Total_income_by_child;
    }

    public void setTotal_rLife_time_Total_income_by_child(String total_rLife_time_Total_income_by_child) {
        Total_rLife_time_Total_income_by_child = total_rLife_time_Total_income_by_child;
    }

    String Total_lifetime;
    String Total_referal_income_by_child;
    String Total_rmonth_total_income_by_child;
    String Total_rone_year_Total_income_by_child;
    String Total_rLife_time_Total_income_by_child;
}
