package com.moneyaan.models;

public class MoonModel {

    String date;
    String moon;
    String rate;
    String refferBy;
    String income;


    public String getRefferBy() {
        return refferBy;
    }

    public void setRefferBy(String refferBy) {
        this.refferBy = refferBy;
    }

    public String getIncome() {
        return income;
    }

    public void setIncome(String income) {
        this.income = income;
    }


    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getMoon() {
        return moon;
    }

    public void setMoon(String moon) {
        this.moon = moon;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }


}
