package com.moneyaan.models;

/**
 * Created by lalji on 28/2/18.
 */

public class SelfRecodModel {

    private String selfTasks;
    private String approvDate;
    private String approvPoints;


    public String getSelfTasks() {
        return selfTasks;
    }

    public void setSelfTasks(String selfTasks) {
        this.selfTasks = selfTasks;
    }

    public String getApprovDate() {
        return approvDate;
    }

    public void setApprovDate(String approvDate) {
        this.approvDate = approvDate;
    }

    public String getApprovPoints() {
        return approvPoints;
    }

    public void setApprovPoints(String approvPoints) {
        this.approvPoints = approvPoints;
    }


}
