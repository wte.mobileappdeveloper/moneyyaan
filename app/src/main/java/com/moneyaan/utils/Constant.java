package com.moneyaan.utils;

public interface Constant {

    String APP_ID_ADS_MOB="ca-app-pub-2539068964968309~5985647677";
    String GLOBAR_ADS_UNIT="ca-app-pub-2539068964968309/";
    String ANDROID_ID="gaid";


    //....Test
    //   ca-app-pub-2539068964968309~5985647677

    String REWARDSESSION="rewardSession";
    String USER_CAST="user_cast";
    String COUNT_DOWN="count_down";
    String SESSION_DATE="session_date";
    String STATUS_VERIFY="status";
    String ADS_REWARD_UNITCODE="ca-app-pub-2539068964968309/1651927631";

    //ca-app-pub-2539068964968309/2402539588



    //test ca-app-pub-3940256099942544/5224354917

    //.............SpecialAds............

    String SPEC_REWARDSESSION="spec_rewardSession";
    String SPECL_COUNT_DOWN="spec_count_down";
    String SPEC_SESSION_DATE="spec_session_date";
    String ADS_INTERSTITUION_UNITCODE="ca-app-pub-2539068964968309/2163773965";
    String TODAY_ATTEMPT_SPEC="today_attempt_spec";
    String TODAY_TASK_STATUS_SPEC="today_task_status_spec";
    //...tca-app-pub-3940256099942544/1033173712

    //...ca-app-pub-3940256099942544/1033173712


    //,,,,,,,,,,,interstitial live ca-app-pub-2539068964968309/8599739700

    //test  ca-app-pub-3940256099942544/8691691433
    //.............BannerAds............

    String BANNER_REWARDSESSION="banner_rewardSession";
    String BANNER_COUNT_DOWN="banner_count_down";
    String BANNER_SESSION_DATE="banner_session_date";
    String ADS_BANNER_UNITCODE="ca-app-pub-2539068964968309/5773283275";
    String TODAY_ATTEMPT_BANNER="today_attempt_banner";
    String TODAY_TASK_STATUS_BANNER="today_task_status_banner";

    //........live  ca-app-pub-2539068964968309/4051570704

    //test  ca-app-pub-3940256099942544/6300978111

    //.............Web URL............

    String WEB_THE_DIRT_SHEET="https://www.dirt-sheet.com";
}
