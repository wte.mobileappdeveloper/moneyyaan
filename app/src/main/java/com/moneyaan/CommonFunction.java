package com.moneyaan;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.provider.Settings;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.regex.Pattern;

/**
 * Created by lalji on 9/4/2017.
 */

public class CommonFunction {
    private static ProgressDialog pd;
    public CommonFunction() {

    }

    public static boolean isValidMail(String email) {
        if (email == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
        }

    }

    public static boolean isValideEmailPattern(String email) {
        String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
        if (email.matches(emailPattern) && email.length() > 0) {
            return true;
        } else {
            return false;
        }

    }

    public static boolean isValidMobile(String phone) {

        boolean check = false;
        if (!Pattern.matches("[a-zA-Z]+", phone)) {
            if (phone.length() < 6 || phone.length() > 13) {
                check = false;
                // Toast.makeText(LoginActivity.this, "Mobile Digit is not Valid", Toast.LENGTH_LONG).show();
            } else {
                check = true;
            }
        } else {
            check = false;
        }

        return check;
    }


//    public  static ProgressDialog showProgressDialog(Activity context,String message,boolean cancelable){
//
//
//        if (null!=context) {
//            pd = new ProgressDialog(context);
//            pd.setCancelable(cancelable);
//            pd.setMessage(message);
//            pd.show();
//
//        }
//
//        return pd;
//    }
//
//    public static ProgressDialog dismissProgressDialog(Activity context){
//
//
//        if (null!=context && null!=pd ) {
//
//            if (!context.isFinishing() && !context.isFinishing() && context.hasWindowFocus()){
//                pd.dismiss();
//            }
//
//        }
//
//
//            return null;
//    }


    public static void reWardDialogPop(final Activity activity,boolean canceleable,String point,String txtStatus) {

        final Dialog  dialog = new Dialog(activity);
        dialog.setContentView(R.layout.bonush_dialog);
        dialog.setCancelable(canceleable);

        TextView txtOutOFDown=dialog.findViewById(R.id.txtOutOFDown);
        TextView txtTastStatus=dialog.findViewById(R.id.txtTastStatus);
        ImageView imgcrossDialog=dialog.findViewById(R.id.imgcrossDialog);

        if (!canceleable){
            imgcrossDialog.setVisibility(View.GONE);
        }

        imgcrossDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        DialogInterface.OnKeyListener dialogWelcomeNavigationOnKey = new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                if (keyCode == android.view.KeyEvent.KEYCODE_BACK) {

                    dialog.dismiss();
                    activity.finish();
                    // move other fragment

                    return true;
                }
                return false;
            }
        };

        dialog.setOnKeyListener(dialogWelcomeNavigationOnKey);


        txtOutOFDown.setText(""+point);
        txtTastStatus.setText(txtStatus);
        dialog.show();


    }

    public static String getCurrentDate()
    {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat mdformat = new SimpleDateFormat("dd/MM/yyyy");
        String newDateStr = mdformat.format(calendar.getTime());


        return newDateStr;
    }
    public static final String md5(final String s) {
        try {
            // Create MD5 Hash
            MessageDigest digest = java.security.MessageDigest
                    .getInstance("MD5");
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            // Create Hex String
            StringBuffer hexString = new StringBuffer();
            for (int i = 0; i < messageDigest.length; i++) {
                String h = Integer.toHexString(0xFF & messageDigest[i]);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
//            Logger.logStackTrace(TAG,e);
        }
        return "";
    }



    public static void startWebView(final Context context,WebView webView, String url, final ProgressBar progressBar) {

        WebSettings settings = webView.getSettings();

        settings.setJavaScriptEnabled(true);
        webView.setScrollBarStyle(View.SCROLLBARS_OUTSIDE_OVERLAY);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setCacheMode(WebSettings.LOAD_DEFAULT);
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }
            @Override
            public void onPageFinished(WebView view, String url) {
                if (progressBar!=null) {
                    progressBar.setVisibility(View.GONE);
                }
            }

            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                Toast.makeText(context, "Error:" + description, Toast.LENGTH_SHORT).show();

            }
        });
        webView.loadUrl(url);
    }



}
