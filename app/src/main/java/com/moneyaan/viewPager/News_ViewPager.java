package com.moneyaan.viewPager;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.moneyaan.R;
import com.moneyaan.models.ViralNewsModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by android on 30/12/17.
 */

public class News_ViewPager extends PagerAdapter {
    private ArrayList<ViralNewsModel> virallist;

    private Context c;
    public  News_ViewPager(Context context, ArrayList<ViralNewsModel> virallist){
        this.c=context;
        this.virallist=virallist;
    }




    @Override
    public int getCount() {
        return virallist.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view==object;
    }


    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {


        LayoutInflater inflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View viewLayout = inflater.inflate(R.layout.cell_expanded_news, container,false);

        ImageView viralimg=viewLayout.findViewById(R.id.expanded_image);
        TextView viraltext=viewLayout.findViewById(R.id.expanded_text);
        TextView title=viewLayout.findViewById(R.id.expanded_title);
        Button fullNews=viewLayout.findViewById(R.id.fullNews);
        TextView auther=viewLayout.findViewById(R.id.author);
        TextView postDate=viewLayout.findViewById(R.id.date);
        final WebView news_web=viewLayout.findViewById(R.id.news_webView);


        ViralNewsModel viralNewsModel=virallist.get(position);




//////////////////////////setting data to views////////////////
        Picasso.with(c).load(viralNewsModel.getUrlToImage()).into(viralimg);
        title.setText(viralNewsModel.getTitle());
        viraltext.setText(viralNewsModel.getDescription());
        postDate.setText("Post Date: "+viralNewsModel.getPublishedAt().substring(0,10).replace("T"," ").replace("+00:00",""));

        if (!viralNewsModel.getAuthor().equalsIgnoreCase("null")){
            auther.setText("Posted by: "+viralNewsModel.getAuthor());

        }
        else {
            auther.setText("Posted by: ");

        }


        news_web.setWebViewClient(new WebViewClient());

        news_web.loadUrl(viralNewsModel.getUrl());
        news_web.getSettings().setJavaScriptEnabled(true);
        news_web.setHorizontalScrollBarEnabled(false);



        fullNews.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final ProgressDialog pd = ProgressDialog.show(c, "", "Please wait...", true);
                pd.setCancelable(true);
                news_web.setWebViewClient(new WebViewClient() {
                    public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
//                Toast.makeText(activity, description, Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onPageStarted(WebView view, String url, Bitmap favicon)
                    {
                        pd.show();
                    }


                    @Override
                    public void onPageFinished(WebView view, String url) {
                        pd.dismiss();


                    }

                });

                news_web.setVisibility(View.VISIBLE);
                viewLayout.findViewById(R.id.linear_layout).setVisibility(View.GONE);
            }
        });


        (container).addView(viewLayout);

        return viewLayout;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {


        ((ViewPager)container).removeView((View)object);
    }

}
