package com.moneyaan;


import android.app.Application;
import android.util.Log;

import com.onesignal.OSPermissionSubscriptionState;
import com.onesignal.OneSignal;

public class MoneYaan extends Application {


    @Override
    public void onCreate() {
        super.onCreate();


        OneSignal.startInit(this)
                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
                .init();

        OSPermissionSubscriptionState status = OneSignal.getPermissionSubscriptionState();
        status.getPermissionStatus().getEnabled();

        boolean s1= status.getSubscriptionStatus().getSubscribed();
        boolean s2=status.getSubscriptionStatus().getUserSubscriptionSetting();
        String s3=status.getSubscriptionStatus().getUserId();
        String s4=status.getSubscriptionStatus().getPushToken();

        Log.e("OneSignalDetails"," "+s3+" "+s2+" "+s1);



    }
}
