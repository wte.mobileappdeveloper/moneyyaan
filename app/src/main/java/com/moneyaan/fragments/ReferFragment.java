package com.moneyaan.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.moneyaan.R;

/**
 * Created by lalji on 21/2/18.
 */

public class ReferFragment extends Fragment {
    View rootView;
    public static RecyclerView refer_recyclerview;
    public static TextView referErnng;
    public static LinearLayout refer_nodata;
    public ReferFragment()
    {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {


        rootView = inflater.inflate(R.layout.referal_task_fragment, container, false);
        refer_recyclerview=rootView.findViewById(R.id.refer_recyclerview);
        referErnng=rootView.findViewById(R.id.total_referBalcAmount);
        refer_nodata=rootView.findViewById(R.id.refer_nodata_layout);

//        selfEarningReport();
        return rootView;
    }


}
