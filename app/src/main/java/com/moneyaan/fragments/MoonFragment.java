package com.moneyaan.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.moneyaan.ApiHandler.VolleySingleton;
import com.moneyaan.LocalSaved.PreferenceHelper;
import com.moneyaan.R;
import com.moneyaan.activities.MonthlyTaskWiseHistory;
import com.moneyaan.configs.Configs;
import com.moneyaan.models.MoonModel;
import com.moneyaan.recyclerAdapter.MoonAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.moneyaan.activities.Home.KEY_USERID;
import static com.moneyaan.configs.Configs.KEY_CLIENT_ID;
import static com.moneyaan.fragments.SelfHistory.self_recyclerview;


public class MoonFragment extends Fragment {


    public RecyclerView refer_recyclerview;
    public TextView referErnng;
    public LinearLayout refer_nodata;
    ProgressDialog progressDialog;

    public MoonFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_moon, container, false);
        refer_recyclerview=rootView.findViewById(R.id.refer_recyclerview);
        referErnng=rootView.findViewById(R.id.total_referBalcAmount);
        refer_nodata=rootView.findViewById(R.id.refer_nodata_layout);

        moonHistory();

        return rootView;
    }

    private void moonHistory()
    {
        progressDialog=new ProgressDialog(getContext());
        progressDialog.setMessage("Loading...");
        progressDialog.show();
        StringRequest request = new StringRequest(Request.Method.POST, Configs.url_Moon_History, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                progressDialog.dismiss();
                try {
                    JSONObject jsonObject=new JSONObject(response);
                    if (!jsonObject.getBoolean("error"))
                    {
                        JSONArray jsonArray=jsonObject.getJSONArray("moonlist");
                        Log.e("MoonList",jsonArray.toString());
                        ArrayList<MoonModel> moonModels=new ArrayList<>();
                        moonModels.clear();
                        for (int i=0;i<jsonArray.length();i++)
                        {
                            MoonModel moonModel=new MoonModel();
                            moonModel.setMoon(jsonArray.getJSONObject(i).getString("moon"));
                            moonModel.setRate(jsonArray.getJSONObject(i).getString("moon_rate"));
                            moonModel.setDate(jsonArray.getJSONObject(i).getString("update_dt"));

                            float incomeRefer=0;
                            float incomeSelf =0;

                            if (!jsonArray.getJSONObject(i).getString("referral_inome").equalsIgnoreCase(null))
                            {
                                incomeRefer=Float.parseFloat(jsonArray.getJSONObject(i).getString("referral_inome"));

                            }

                            if (jsonArray.getJSONObject(i).getString("self_income").equalsIgnoreCase(null))
                            {
                                incomeSelf=Float.parseFloat(jsonArray.getJSONObject(i).getString("self_income"));
                            }

                            float totalIncome=incomeRefer+incomeSelf;

                            moonModel.setIncome(""+totalIncome);
                            moonModel.setRefferBy(jsonArray.getJSONObject(i).getString("refer_by"));
                            moonModels.add(moonModel);
                        }

                        MoonAdapter moonAdapter=new MoonAdapter(getContext(),moonModels);
                        refer_recyclerview.setAdapter(moonAdapter);
                        refer_recyclerview.setLayoutManager(new LinearLayoutManager(getContext()));
                        refer_recyclerview.setHasFixedSize(true);
                    }else {
                        refer_nodata.setVisibility(View.VISIBLE);
                        refer_recyclerview.setVisibility(View.INVISIBLE);
                    }

                }catch (JSONException e)
                {
                    refer_nodata.setVisibility(View.VISIBLE);
                    refer_recyclerview.setVisibility(View.INVISIBLE);
                }

            }
        },new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                refer_nodata.setVisibility(View.VISIBLE);
                refer_recyclerview.setVisibility(View.INVISIBLE);
                Toast.makeText(getContext(), "Something Server Error", Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        }){
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                String userId=getContext().getSharedPreferences("login", Context.MODE_PRIVATE).getString("User_Id", "");
                params.put(KEY_CLIENT_ID,Configs.clientId(getContext()));
                params.put(KEY_USERID, userId);
                Log.e("Approvehist params",params.toString());
                return params;
            }
        };

        VolleySingleton.getInstance(getContext()).addToRequestQueue(request);
    }

}
