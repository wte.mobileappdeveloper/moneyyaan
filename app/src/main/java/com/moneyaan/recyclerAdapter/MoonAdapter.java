package com.moneyaan.recyclerAdapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.moneyaan.R;
import com.moneyaan.models.MoonModel;

import java.util.ArrayList;

public class MoonAdapter extends RecyclerView.Adapter<MoonAdapter.ViewHolder>{

    private Context context;
    private ArrayList<MoonModel> moonModelArrayList;

    public MoonAdapter(Context context, ArrayList<MoonModel> arrayList)
    {
        this.context=context;
        this.moonModelArrayList=arrayList;
    }

    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view= LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.moon_row_layout,viewGroup,false);
        return new MoonAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int i) {

        Log.e("Data",moonModelArrayList.get(i).getIncome() +" "+moonModelArrayList.get(i).getRefferBy()+""+moonModelArrayList.get(i).getIncome() );

        holder.txt_reffer_by.setText(moonModelArrayList.get(i).getRefferBy());
        holder.txt_Moon.setText(moonModelArrayList.get(i).getMoon());
        holder.txt_date.setText(moonModelArrayList.get(i).getDate());
        holder.txt_point.setText(moonModelArrayList.get(i).getIncome());
        holder.txt_rate.setText(moonModelArrayList.get(i).getRate());

    }

    @Override
    public int getItemCount() {
        return moonModelArrayList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        TextView txt_point,txt_date,txt_reffer_by,txt_Moon,txt_rate;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            txt_point=itemView.findViewById(R.id.txt_point);
            txt_date=itemView.findViewById(R.id.txt_date);
            txt_reffer_by=itemView.findViewById(R.id.txt_reffer_by);
            txt_Moon=itemView.findViewById(R.id.txt_Moon);
            txt_rate=itemView.findViewById(R.id.txt_rate);


        }
    }
}
