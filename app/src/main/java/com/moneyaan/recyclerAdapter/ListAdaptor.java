package com.moneyaan.recyclerAdapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.moneyaan.R;
import com.moneyaan.models.ListNotifyModel;

import java.util.ArrayList;

/**
 * Created by ErLalji Yadav on 7/17/2017.
 */

public class ListAdaptor extends BaseAdapter {

    String[] notifyResult;
    Context context;
    String[] dateNotifyresult;
    ArrayList<ListNotifyModel> resultArray;
    private static LayoutInflater inflater = null;

    public ListAdaptor(Context mainActivity, ArrayList<ListNotifyModel> resultArrayData) {


        context = mainActivity;
        this.resultArray = resultArrayData;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return resultArray.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup parent) {
        Holder holder = new Holder();
        final ListNotifyModel position = resultArray.get(i);
        View rowView;
        rowView = inflater.inflate(R.layout.list_row, null);
        holder.notifyMsg = (TextView) rowView.findViewById(R.id.notifymsg);
        holder.notifyDate = (TextView) rowView.findViewById(R.id.notify_Date);

        holder.notifyMsg.setText(position.getNotifyMsg());
        holder.notifyDate.setText(position.getNotifyDate());

        return rowView;
    }

    public class Holder {
        TextView notifyMsg;
        TextView notifyDate;

    }

}
