package com.moneyaan.recyclerAdapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.moneyaan.R;
import com.moneyaan.configs.Configs;
import com.moneyaan.models.TeamDetailsModal;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.moneyaan.activities.TeamsDetails.KEY_USER_ID;
import static com.moneyaan.activities.TeamsDetails.linearLayoutLevelWiseData;
import static com.moneyaan.activities.TeamsDetails.linearLayoutTotalData;
import static com.moneyaan.activities.TeamsDetails.teamView;
import static com.moneyaan.activities.TeamsDetails.total_levelWise_income;
import static com.moneyaan.configs.Configs.KEY_CLIENT_ID;

/**
 * Created by android on 30/12/17.
 */

public class TeamAdapter extends RecyclerView.Adapter<TeamAdapter.Holder> {


    Context context;
    private ArrayList<TeamDetailsModal> teamDetailsModals;
    private String type;

    private double sum=0.0f;



//    --------------------------------to hide level 2 mobile
    private String dontShow="dontShow";


    public TeamAdapter(Context context, ArrayList<TeamDetailsModal> teamDetailsModals,String type){

        this.context=context;
        this.type=type;
        this.teamDetailsModals=teamDetailsModals;
    }


    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_level_wise_users,parent,false);



        return new TeamAdapter.Holder(view);

    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {



            TeamDetailsModal detailsModal=teamDetailsModals.get(position);

            if (type.equalsIgnoreCase("all")){
                holder.level.setText(detailsModal.getLevel());
                holder.numberOfUsers.setText(detailsModal.getNo_of_user());

                holder.itemView.findViewById(R.id.layout_all).setVisibility(View.VISIBLE);
                holder.itemView.findViewById(R.id.levelWise).setVisibility(View.GONE);

            }
            else {
                holder.name.setText(detailsModal.getName());
                holder.email.setText(detailsModal.getEmail());
                holder.mobile.setText(detailsModal.getMobile());
                holder.amount.setText(detailsModal.getReferral_amount());

                if (detailsModal.getMobile().equalsIgnoreCase(dontShow)){
                    holder.itemView.findViewById(R.id.view_bw_contctDetails).setVisibility(View.GONE);
                    holder.mobile.setVisibility(View.GONE);
                }else {
                    holder.itemView.findViewById(R.id.view_bw_contctDetails).setVisibility(View.VISIBLE);
                    holder.mobile.setVisibility(View.VISIBLE);
                }

                holder.itemView.findViewById(R.id.layout_all).setVisibility(View.GONE);
                holder.itemView.findViewById(R.id.levelWise).setVisibility(View.VISIBLE);

            }







    }

    @Override
    public int getItemCount() {

       return teamDetailsModals.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        TextView level,numberOfUsers,name,email,amount,mobile;

        public Holder(View itemView) {
            super(itemView);
            level=itemView.findViewById(R.id.txt_level);
            numberOfUsers=itemView.findViewById(R.id.txt_no_of_user);
            name=itemView.findViewById(R.id.name);
            email=itemView.findViewById(R.id.email);
            amount=itemView.findViewById(R.id.amount);
            mobile=itemView.findViewById(R.id.mobile);


            level.setSelected(true);
            numberOfUsers.setSelected(true);
            name.setSelected(true);
            email.setSelected(true);
            amount.setSelected(true);
            mobile.setSelected(true);


            level.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    teamdetailsLevelWise(level.getText().toString().replace(" ",""));
                }
            });


        }
    }



    private void teamdetailsLevelWise(final String level) {

        final ProgressDialog progressDialog=new ProgressDialog(context);
        progressDialog.setMessage("Loading..");
        progressDialog.setCancelable(false);
        progressDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Configs.url_teamDetailsLevelWise,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("teamdetails response",response);

                        try {
                            JSONObject jObject = new JSONObject(response);

                            if (jObject.getString("error").equalsIgnoreCase("false")){
                                ArrayList<TeamDetailsModal> teamsDetailsArrayList=new ArrayList<>();
                                teamView.setVisibility(View.VISIBLE);


                                JSONArray jsonArray=jObject.getJSONArray("customerdata");
                                for (int i=0;i<jsonArray.length();i++){
                                    JSONObject jsonObject=jsonArray.getJSONObject(i);


//                                    -----------------------------to show mobile for first level------------------------------------
                                    if (level.equalsIgnoreCase("Level1")){
                                        teamsDetailsArrayList.add(new TeamDetailsModal(jsonObject.getString("name"),jsonObject.getString("email"),jsonObject.getString("referral_amount"),jsonObject.getString("mobile")));;

                                    }else {
                                        teamsDetailsArrayList.add(new TeamDetailsModal(jsonObject.getString("name"),jsonObject.getString("email"),jsonObject.getString("referral_amount"),dontShow));;

                                    }

                                    sum=sum+Double.parseDouble(jsonObject.getString("referral_amount"));
                                    float total=Float.parseFloat(new DecimalFormat(".##").format(sum));
                                    total_levelWise_income.setText("Lifetime earning from Level "+level.substring(5)+"  : "+total);
                                }
                                linearLayoutLevelWiseData.setVisibility(View.VISIBLE);
                                linearLayoutTotalData.setVisibility(View.GONE);

                                TeamAdapter teamAdapter=new TeamAdapter(context,teamsDetailsArrayList,"level");
                                teamView.setLayoutManager(new LinearLayoutManager(context.getApplicationContext()));
                                teamView.setHasFixedSize(true);
                                teamView.setAdapter(teamAdapter);
                                teamAdapter.notifyDataSetChanged();
                            }

                            else {
                                teamView.setVisibility(View.GONE);

                            }





                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.e("exep teamdet",e.toString());
                        }

                        progressDialog.dismiss();
                    }
                },
                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();

                        Log.e("Server Side Erorr",error.toString());

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
//                SharedPreferences preferences = getSharedPreferences("login", Context.MODE_PRIVATE);
                String userId = context.getSharedPreferences("login", Context.MODE_PRIVATE).getString("User_Id", "");
                params.put(KEY_CLIENT_ID,Configs.clientId(context));
                params.put(KEY_USER_ID,userId);
                params.put("level",level);
                return params;
            }

        };

        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(stringRequest);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(100000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

    }

}
