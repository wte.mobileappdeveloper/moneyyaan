package com.moneyaan.recyclerAdapter;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;
import android.widget.TextView;

import com.moneyaan.R;
import com.moneyaan.activities.ViralNews;
import com.moneyaan.models.ViralNewsModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by android on 30/12/17.
 */

public class HomeRecyclerAdapter_viralNews extends RecyclerView.Adapter<HomeRecyclerAdapter_viralNews.Holder> {
    private int lastPosition = -1;

    private ArrayList<ViralNewsModel> virallist=new ArrayList<>();

    Context context;
    public HomeRecyclerAdapter_viralNews(Context context){

        this.context=context;
    }

    public HomeRecyclerAdapter_viralNews(Context context, ArrayList<ViralNewsModel> virallist) {
        this.virallist=virallist;
        this.context=context;

    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_viral_news_home,parent,false);

        return new HomeRecyclerAdapter_viralNews.Holder(view);

    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onBindViewHolder(Holder holder, int position) {


        ViralNewsModel viralNewsModel=virallist.get(position);


        holder.author=viralNewsModel.getAuthor();
        holder.description=viralNewsModel.getDescription();
        holder.publishedAt=viralNewsModel.getPublishedAt();
        holder.title=viralNewsModel.getTitle();
        holder.url=viralNewsModel.getUrl();
        holder.urlToImage=viralNewsModel.getUrlToImage();
//        Log.e("ViralImg",""+position);


        holder.news_text.setText(holder.title);
        if (viralNewsModel.getUrlToImage().isEmpty())
        {
            Picasso.with(context)
                    .load(R.drawable.place_holder)
                    .placeholder(R.drawable.place_holder)
                    .error(R.drawable.place_holder)
                    .into(holder.imagenews); //this is your ImageView
        }else {
            Picasso.with(context)
                    .load(holder.urlToImage)
                    .placeholder(R.drawable.place_holder)
                    .error(R.drawable.place_holder)
                    .into(holder.imagenews); //this is your ImageView
        }




    }



    @Override
    public int getItemCount() {
        return virallist.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        ImageView imagenews,share;
        TextView news_text;

        String author,title,description,url,urlToImage,publishedAt;
        public Holder(View itemView) {
            super(itemView);
            imagenews=itemView.findViewById(R.id.news_image);
            news_text=itemView.findViewById(R.id.news_text);

            itemView.findViewById(R.id.viralnews_cell_card).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent=new Intent(context, ViralNews.class);


                    intent.putExtra("position",getAdapterPosition());
                    intent.putExtra("virallist",virallist);

//                    intent.putExtra("author",author);
//                    intent.putExtra("title",title);
//                    intent.putExtra("description",description);
//                    intent.putExtra("url",url);
//                    intent.putExtra("urlToImage",urlToImage);
//                    intent.putExtra("publishedAt",publishedAt);

                    context.startActivity(intent);
                }
            });

            itemView.findViewById(R.id.share_news).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                        sharingIntent.setType("text/plain");
                        sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "IGYAAN");
                        sharingIntent.putExtra(Intent.EXTRA_TEXT, "Click on the link below to read the\n" +
                                "story\n" +title+"\n"+ url);
                        context.startActivity(Intent.createChooser(sharingIntent, "Sharing Options"));

                    } catch (Exception e) {
                        Log.e("sharenewserror",e.toString());
                        e.printStackTrace();
                    }
                }
            });



        }

    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void animate(View view) {
        view.animate().cancel();
        view.setTranslationZ(0);
        view.setAlpha(0);
        view.animate().alpha(1.0f).translationZ(100).setDuration(1000);
    }



    private void setAnimation(View viewToAnimate, int position) {
        // If the bound view wasn't previously displayed on screen, it's animated
//        if (position > lastPosition) {
            ScaleAnimation anim = new ScaleAnimation(0.0f, 1.0f, 0.0f, 1.0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
            anim.setDuration(500);//to make duration random number between [0,501)
            viewToAnimate.startAnimation(anim);
//            lastPosition = position;
//        }
    }
}
