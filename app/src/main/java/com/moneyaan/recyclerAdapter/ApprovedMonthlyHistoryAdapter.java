package com.moneyaan.recyclerAdapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.moneyaan.R;
import com.moneyaan.activities.MonthlyTaskWiseHistory;
import com.moneyaan.models.ApprovedMonthlyHistoryModel;

import java.util.ArrayList;

/**
 * Created by android on 30/12/17.
 */

public class ApprovedMonthlyHistoryAdapter extends RecyclerView.Adapter<ApprovedMonthlyHistoryAdapter.Holder> {


    Context context;
    ArrayList<ApprovedMonthlyHistoryModel> approvedMonthlyHistoryModelArrayList=new ArrayList<>();


    public ApprovedMonthlyHistoryAdapter(Context context, ArrayList<ApprovedMonthlyHistoryModel> approvedMonthlyHistoryModelArrayList){

        this.approvedMonthlyHistoryModelArrayList=approvedMonthlyHistoryModelArrayList;
        this.context=context;
    }


    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_approved_monthly_history,parent,false);

        Log.e("monthly adapder","called");

        return new ApprovedMonthlyHistoryAdapter.Holder(view);

    }

    @Override
    public void onBindViewHolder(Holder holder, int i) {


        final ApprovedMonthlyHistoryModel position = approvedMonthlyHistoryModelArrayList.get(i);
        holder.month.setText(position.getMonthYear());
        holder.self.setText(position.getSelfincome());
        holder.total.setText(position.getMonthwisetotal());
        holder.refer.setText(position.getIncomebychild());
        Log.e("monthly ader bindholder",String.valueOf(i));

        holder.setIsRecyclable(false);

    }

    @Override
    public int getItemCount() {

        Log.e("monthly adater size",String.valueOf(approvedMonthlyHistoryModelArrayList.size()));
        return approvedMonthlyHistoryModelArrayList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        TextView month,self,total,refer;

        public Holder(View itemView) {
            super(itemView);
            month=itemView.findViewById(R.id.txt_month);
            self=itemView.findViewById(R.id.txt_self_work_month);
            total=itemView.findViewById(R.id.txt_total_work_month);

            refer=itemView.findViewById(R.id.txt_refer_income);

            month.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent=new Intent(context, MonthlyTaskWiseHistory.class);
                    intent.putExtra("month",month.getText().toString());
                    intent.putExtra("status","P");
                    context.startActivity(intent);
                }
            });


        }
    }
}
