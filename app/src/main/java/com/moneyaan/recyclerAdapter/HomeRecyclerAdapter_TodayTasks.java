package com.moneyaan.recyclerAdapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.moneyaan.R;
import com.moneyaan.activities.EarnOffers;
import com.moneyaan.models.TodayTaskModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by android on 26/12/17.
 */

public class HomeRecyclerAdapter_TodayTasks extends RecyclerView.Adapter<HomeRecyclerAdapter_TodayTasks.Holder> {


    private ArrayList<TodayTaskModel> listTodayTask;
    private Context context;


    public HomeRecyclerAdapter_TodayTasks(Context context,ArrayList<TodayTaskModel> listTodayTask){
        this.listTodayTask=listTodayTask;
        this.context=context;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_today_task_new,parent,false);

        return new HomeRecyclerAdapter_TodayTasks.Holder(view);






    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onBindViewHolder(Holder holder, final int position) {




///////////////////////////set random colors//////////////////////////

        List<String> colors;

        colors=new ArrayList<String>();
        colors.clear();

        colors.add("#00BFA5");
        colors.add("#009688");
        colors.add("#FBC02D");
        colors.add("#FF8A65");
        colors.add("#FF3D00");
        colors.add("#A1887F");

//        colors.add(context.getResources().getString(0+R.color.trans_orange));
//        colors.add(context.getResources().getString(0+R.color.trans_blue));
//        colors.add(context.getResources().getString(0+R.color.trans_green));
//        colors.add(context.getResources().getString(0+R.color.trans_red));


        try {
            Random r = new Random();
            int i1 = r.nextInt(6);
            GradientDrawable draw = new GradientDrawable();
            draw.setColor(Color.parseColor(colors.get(i1)));
            draw.setCornerRadius(3);
            holder.executeTask.setBackground(draw);
        }catch (Exception e){

        }


        final TodayTaskModel i = listTodayTask.get(position);
//        holder.txt_Amz.setText(i.getPackage_Name());
        holder.button_text = i.getPackage_Name();
        holder.work_url = i.getWork_url();
        holder.package_id = i.getPackage_id();
        holder.url_webView = i.getWebUrl();
        holder.desc.setText(i.getAppDesc());
//        holder.txt_subDesc.setText(i.getAppSdesc());
//        holder.txt_amt_crdt.setText(i.getAmount_Credit());
        holder.str_txt_credit_amt=i.getAmount_Credit();
        holder.url_icons = i.getIcon();
        holder.show_point=i.getShowPoint();
        holder.btn_install.setText(i.getShowPoint());
        holder.ClickBLabel=i.getClickBLabel();
        holder.btn_install.setText(holder.button_text);
        holder.full_desc=i.getAppDesc();
        holder.short_desc=i.getAppSdesc();
        holder.pause=i.getPause();


        holder.full_desc_hindi=i.getAppDesc_hindi();
        holder.short_desc_hindi=i.getAppSdesc_hindi();
        holder.work_url_hindi=i.getWebUrl_hindi();

        holder.points.setText(holder.str_txt_credit_amt);


        Log.e("taskdec",holder.url_webView);



        if (holder.url_icons.isEmpty()) { //url.isEmpty()
            Picasso.with(context)
                    .load(R.drawable.place_holder)
                    .placeholder(R.drawable.place_holder)
                    .error(R.drawable.place_holder)
                    .into(holder.image);

        } else {
            Picasso.with(context)
                    .load(listTodayTask.get(position).getIcon())
                    .placeholder(R.drawable.place_holder)
                    .error(R.drawable.place_holder)
                    .into(holder.image); //this is your ImageView
        }

    }

    @Override
    public int getItemCount() {
        return listTodayTask.size();
    }



    public class Holder extends RecyclerView.ViewHolder {
        ImageView image;
        TextView desc;
        String button_text, package_id;
        String show_point;

        String work_url,str_txt_credit_amt,pause;
        String url_icons;
        String url_webView;
        String ClickBLabel;
        TextView btn_install,points;
        RelativeLayout executeTask;
        CardView card;
        String full_desc;
        String short_desc;
        String full_desc_hindi;
        String short_desc_hindi;
        String work_url_hindi;

        public Holder(final View itemView) {
            super(itemView);


            executeTask=itemView.findViewById(R.id.executetask_home_recycler);
            image=itemView.findViewById(R.id.home_rec_image);
            desc=itemView.findViewById(R.id.home_rec_text);
            btn_install=itemView.findViewById(R.id.home_rec_button_text);
            card=itemView.findViewById(R.id.home_card);
            points=itemView.findViewById(R.id.txt_Credit);
            points.setSelected(true);






////////////call flip image animation//////////
//            flip();



            card.setOnClickListener(new View.OnClickListener() {
                @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
                @Override
                public void onClick(View view) {




                    Bundle bundle = new Bundle();
                    Intent intent = new Intent(context, EarnOffers.class);

                    //  Toast.makeText(v.getContext(),"Open New Fragment",Toast.LENGTH_LONG).show();

                    bundle.putString("txt_Amz", button_text);
                    bundle.putString("url_webView", url_webView);
                    bundle.putString("package_id", package_id);
                    bundle.putString("work_url", work_url);
                    bundle.putString("icon", url_icons);
                    bundle.putString("txt_Credit",str_txt_credit_amt);
                    bundle.putString("ClickBLabel",ClickBLabel);
                    bundle.putString("full_desc",full_desc);
                    bundle.putString("short_desc",short_desc);
                    bundle.putString("full_desc_hindi",full_desc_hindi);
                    bundle.putString("short_desc_hindi",short_desc_hindi);
                    bundle.putString("work_url_hindi",work_url_hindi);
                    bundle.putString("pause",pause);

                    intent.putExtras(bundle);

                    intent.putExtra("activity","todaytask");
                    context.startActivity(intent);

                }
            });
        }
    }}
