package com.moneyaan.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.support.design.widget.Snackbar;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.moneyaan.R;
import com.moneyaan.configs.Configs;
import com.moneyaan.utils.InternetConnectivity.BaseActivity;
import com.moneyaan.utils.InternetStatus;
import com.moneyaan.CommonFunction;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.moneyaan.configs.Configs.KEY_CLIENT_ID;
import static com.moneyaan.configs.Configs.clientId;

public class ForgetPassword extends BaseActivity {
    private TextView txt_login, txt_Sign_Up;
    private EditText forget_mobile;
    private Button btn_forget_submit;
    public static final String KEY_MOBILE = "mobile";
    ProgressDialog progressDoalog;
    Toolbar toolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_password);
        toolbar = (Toolbar) findViewById(R.id.toggle_toolbar);
        toolbar.setTitle("Forget Password");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        txt_login = (TextView) findViewById(R.id.txt_forget_login);
        txt_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent inn = new Intent(ForgetPassword.this, Login.class);
                startActivity(inn);
                finish();
            }
        });

        txt_Sign_Up = (TextView) findViewById(R.id.txt_forget_signup);
        txt_Sign_Up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent inn = new Intent(ForgetPassword.this, SignUp.class);
                startActivity(inn);
                finish();
            }
        });

        forget_mobile= (EditText) findViewById(R.id.edt__forget_mobile);

        btn_forget_submit= (Button) findViewById(R.id.btn_forget_submit);
        btn_forget_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loginValidation();
            }
        });
    }





    ///////////////////////////validation///////////////////////////
    private void loginValidation()
    {
        if (forget_mobile.getText().toString() == null || forget_mobile.getText().length() <10 || CommonFunction.isValidMobile(forget_mobile.getText().toString()) == false) {
            Toast.makeText(this, "Enter Valid Mobile Number", Toast.LENGTH_SHORT).show();
            forget_mobile.requestFocus();
        }else
        {
            if (InternetStatus.getInstance(this).isOnline()) {
                forgetApi();

            } else {
                String message;
                Snackbar snackbar;
                int color;
                color = Color.RED;
                message = "Sorry Check Your Internet Connection !!";

                snackbar = Snackbar.make(findViewById(R.id.login_relative_layout), message, Snackbar.LENGTH_SHORT);
                View sbView = snackbar.getView();
                TextView textView = sbView.findViewById(android.support.design.R.id.snackbar_text);
                textView.setTextColor(color);
                snackbar.show();

            }
        }
    }


    //...........................................forget api...................................//
    private void forgetApi()
    {
        final String  strMobile= forget_mobile.getText().toString();
        progressDoalog = new ProgressDialog(this);
        progressDoalog.setMessage("Sending...");
        progressDoalog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Configs.url_ForgetApi, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {


                try {
                    Log.e("forgetResponse0",response);
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.optString("error").equalsIgnoreCase("false")||jsonObject.optString("error_msg").equalsIgnoreCase("Password Sent"))
                    {
                        Toast.makeText(ForgetPassword.this,"Successfully Update",Toast.LENGTH_SHORT).show();
                        progressDoalog.dismiss();
                        startActivity(new Intent(ForgetPassword.this,Login.class));
                        finish();
                    }else if(jsonObject.optString("error").equalsIgnoreCase("true"))
                    {
                        Toast.makeText(ForgetPassword.this,"This number is not registered",Toast.LENGTH_SHORT).show();
                        progressDoalog.dismiss();
                    }

                }catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        },new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDoalog.dismiss();
                Toast.makeText(ForgetPassword.this,"Some error occurred",Toast.LENGTH_SHORT).show();

            }
        }){
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put(KEY_MOBILE, strMobile);
                params.put(KEY_CLIENT_ID, clientId(ForgetPassword.this));

                return params;
            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


    }

}
