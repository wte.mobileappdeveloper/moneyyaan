package com.moneyaan.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.moneyaan.R;
import com.moneyaan.configs.Configs;
import com.moneyaan.models.TeamDetailsModal;
import com.moneyaan.recyclerAdapter.TeamAdapter;
import com.moneyaan.utils.InternetConnectivity.BaseActivity;

import org.json.JSONException;
import org.json.JSONObject;

import static com.moneyaan.configs.Configs.KEY_CLIENT_ID;

public class TeamsDetails extends BaseActivity {


    public static final String KEY_USER_ID = "customer_id";
    public static LinearLayout linearLayoutLevelWiseData,linearLayoutTotalData;
    TextView marqueTextReferamt;
    public static TextView total_levelWise_income;

    ProgressDialog progressDialog;
    String userId;
    public  static RecyclerView teamView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teams_details);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toggle_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Team Details");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        teamView=findViewById(R.id.team_view);
        progressDialog=new ProgressDialog(this);
        progressDialog.setMessage("Please Wait..");
        progressDialog.setCancelable(false);
        linearLayoutLevelWiseData=findViewById(R.id.textBarexpand);
        linearLayoutTotalData=findViewById(R.id.textBar);
        marqueTextReferamt=findViewById(R.id.marqueText);
        marqueTextReferamt.setSelected(true);
        total_levelWise_income=findViewById(R.id.total_levelWise_income);

        teamdetailsLevel();




    }


    public void teamdetailsLevel() {

        progressDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Configs.url_teamDetails,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("teamdetails response",response);

                        try {
                            JSONObject jObject = new JSONObject(response);

                            if (jObject.getString("error").equalsIgnoreCase("false")){
                                ArrayList<TeamDetailsModal> teamsDetailsArrayList=new ArrayList<>();
                                teamView.setVisibility(View.VISIBLE);
                                linearLayoutLevelWiseData.setVisibility(View.GONE);
                                linearLayoutTotalData.setVisibility(View.VISIBLE);

                                teamsDetailsArrayList.add(new TeamDetailsModal("Level 1",String.valueOf(jObject.getInt("Level 1"))));
                                teamsDetailsArrayList.add(new TeamDetailsModal("Level 2",String.valueOf(jObject.getInt("Level 2"))));
                                teamsDetailsArrayList.add(new TeamDetailsModal("Level 3",String.valueOf(jObject.getInt("Level 3"))));
                                teamsDetailsArrayList.add(new TeamDetailsModal("Level 4",String.valueOf(jObject.getInt("Level 4"))));
                                teamsDetailsArrayList.add(new TeamDetailsModal("Level 5",String.valueOf(jObject.getInt("Level 5"))));
                                teamsDetailsArrayList.add(new TeamDetailsModal("Level 6",String.valueOf(jObject.getInt("Level 6"))));
                                teamsDetailsArrayList.add(new TeamDetailsModal("Level 7",String.valueOf(jObject.getInt("Level 7"))));

                                TeamAdapter teamAdapter=new TeamAdapter(TeamsDetails.this,teamsDetailsArrayList,"all");
                                teamView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                                teamView.setHasFixedSize(true);
                                teamView.setAdapter(teamAdapter);
                                teamAdapter.notifyDataSetChanged();

                            }

                            else {
                                teamView.setVisibility(View.GONE);

                            }





                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.e("exep teamdet",e.toString());
                        }

                        progressDialog.dismiss();
                    }
                },
                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();

                        Log.e("Server Side Erorr",error.toString());

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                userId = getSharedPreferences("login", Context.MODE_PRIVATE).getString("User_Id", "");
                params.put(KEY_CLIENT_ID,Configs.clientId(TeamsDetails.this));
                params.put(KEY_USER_ID,userId);
                return params;
            }

        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(100000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

    }


    @Override
    public void onBackPressed() {


        if (progressDialog.isShowing()){
            progressDialog.dismiss();
        }

        if (linearLayoutLevelWiseData.getVisibility()==View.VISIBLE){
            teamdetailsLevel();


        }else {
            super.onBackPressed();
            overridePendingTransition( R.anim.ritht_to_left_in, R.anim.ritht_to_left );
        }

    }

}
