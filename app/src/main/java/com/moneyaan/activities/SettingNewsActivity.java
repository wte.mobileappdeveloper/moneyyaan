package com.moneyaan.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;

import com.moneyaan.R;
import com.moneyaan.utils.InternetConnectivity.BaseActivity;

public class SettingNewsActivity extends BaseActivity {
    SharedPreferences.Editor editor;
    SharedPreferences myPrefsNews;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting_news);

//        ///////////////toolbar////////////////////////
        Toolbar toolbar = (Toolbar) findViewById(R.id.toggle_toolbar);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        myPrefsNews = getSharedPreferences("newsFilter", Context.MODE_PRIVATE);
       editor = myPrefsNews.edit();
        findViewById(R.id.medical).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editor.putString("category", "medical-news-today");
                editor.commit();
                Toast.makeText(SettingNewsActivity.this,"Category : Medical",Toast.LENGTH_SHORT).show();

                Intent intent=new Intent(SettingNewsActivity.this,Home.class);
                intent.putExtra("focus",1);

                startActivity(intent);
            }
        });
        findViewById(R.id.topNews).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editor.putString("category", "google-news-in");
                editor.commit();
                Toast.makeText(SettingNewsActivity.this,"Category : Top News",Toast.LENGTH_SHORT).show();

                Intent intent=new Intent(SettingNewsActivity.this,Home.class);
                intent.putExtra("focus",1);

                startActivity(intent);            }
        });
        findViewById(R.id.cricket).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editor.putString("category", "espn-cric-info");
                editor.commit();
                Toast.makeText(SettingNewsActivity.this,"Category : Cricket",Toast.LENGTH_SHORT).show();

                Intent intent=new Intent(SettingNewsActivity.this,Home.class);
                intent.putExtra("focus",1);

                startActivity(intent);
            }
        });
        findViewById(R.id.entertain).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editor.putString("category", "buzzfeed");
                editor.commit();
                Toast.makeText(SettingNewsActivity.this,"Category : Entertainment",Toast.LENGTH_SHORT).show();

                Intent intent=new Intent(SettingNewsActivity.this,Home.class);
                intent.putExtra("focus",1);

                startActivity(intent);
            }
        });
    }
}
