package com.moneyaan.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.CountDownTimer;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.moneyaan.ApiHandler.VolleySingleton;
import com.moneyaan.CommonFunction;
import com.moneyaan.LocalSaved.PreferenceHelper;
import com.moneyaan.R;
import com.moneyaan.configs.Configs;
import com.moneyaan.utils.InternetConnectivity.BaseActivity;
import com.startapp.android.publish.adsCommon.StartAppAd;

import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import static com.moneyaan.CommonFunction.getCurrentDate;
import static com.moneyaan.configs.Configs.KEY_CLIENT_ID;
import static com.moneyaan.configs.Configs.clientId;
import static com.moneyaan.utils.Constant.ANDROID_ID;
import static com.moneyaan.utils.Constant.GLOBAR_ADS_UNIT;
import static com.moneyaan.utils.Constant.SPECL_COUNT_DOWN;
import static com.moneyaan.utils.Constant.SPEC_REWARDSESSION;
import static com.moneyaan.utils.Constant.SPEC_SESSION_DATE;
import static com.moneyaan.utils.Constant.TODAY_ATTEMPT_SPEC;
import static com.moneyaan.utils.Constant.TODAY_TASK_STATUS_SPEC;
import static com.moneyaan.utils.Constant.USER_CAST;
import static com.moneyaan.utils.Constant.WEB_THE_DIRT_SHEET;

public class SpecializeActivity extends BaseActivity {

    private Toolbar toolbar;
    public static final String KEY_USER_ID = "customer_id";
    private ImageView imgRightArrow,img_timer,imgLeftArrow;
    private CountDownTimer countDownTimer;
    private TextView txtCountDown,txtOutOFDown,txtCountCircleDown;
    int i=0;
    private RelativeLayout lytCount;
    private WebView mWebview ;
    String _userId;
    boolean isVideoCompleted=true;
    boolean isCounterFinished=true;
    boolean isReplay=true;
    private ProgressDialog progressDialog;
    private ProgressBar webLoading;

    int numberOfAttempt=1;
    StartAppAd startAppAd;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_specialize);

        toolbar= findViewById(R.id.toggle_toolbar);
        TextView toolactName= findViewById(R.id.act_name);
        toolactName.setText("Interstitial");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        webLoading=findViewById(R.id.webLoading);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        startAppAd = new StartAppAd(this);

     //   numberOfAdsActive();

        // this is for admob
//        mAdView=(AdView) findViewById(R.id.adView);

       // bannerAdLoad();

        SharedPreferences preferences = getSharedPreferences("login", Context.MODE_PRIVATE);
        _userId = preferences.getString("User_Id", "");

        mWebview  = findViewById(R.id.webTopNews);

        CommonFunction.startWebView(SpecializeActivity.this,mWebview,WEB_THE_DIRT_SHEET,webLoading);


        imgRightArrow=findViewById(R.id.imgRightArrow);
        img_timer=findViewById(R.id.img_timer);
        imgLeftArrow=findViewById(R.id.imgLeftArrow);
        txtCountDown=findViewById(R.id.txtCountDown);
        txtOutOFDown=findViewById(R.id.txtOutOFDown);
        txtCountCircleDown=findViewById(R.id.txtCountCircleDown);
        lytCount=findViewById(R.id.lytCount);

        numberOfAttempt=Integer.parseInt(PreferenceHelper.getStringPreference(SpecializeActivity.this,TODAY_ATTEMPT_SPEC));
        i=Integer.parseInt(PreferenceHelper.getStringPreference(SpecializeActivity.this,SPECL_COUNT_DOWN));

        if (PreferenceHelper.getStringPreference(SpecializeActivity.this,TODAY_TASK_STATUS_SPEC).equalsIgnoreCase("false"))
        {
            if (numberOfAttempt<50)
            {
                if (PreferenceHelper.getStringPreference(SpecializeActivity.this,SPECL_COUNT_DOWN).equals("20"))
                {
                    txtOutOFDown.setText("Done");
                    txtCountDown.setText("Today task has been completed !");
                    txtCountDown.setTextColor(getResources().getColor(R.color.material_green));

                }else {

                    txtOutOFDown.setText(PreferenceHelper.getStringPreference(SpecializeActivity.this,SPECL_COUNT_DOWN)+"/"+20);

                }

                if (!PreferenceHelper.isGetFlag(SpecializeActivity.this,SPEC_REWARDSESSION))
                {
                    CommonFunction.reWardDialogPop(SpecializeActivity.this,true,PreferenceHelper.getStringPreference(SpecializeActivity.this,SPECL_COUNT_DOWN)+"/20","Today task is incomplete !");

                }else {

                    CommonFunction.reWardDialogPop(SpecializeActivity.this,true,"Done","Today task has been completed !");
                    imgRightArrow.setVisibility(View.INVISIBLE);
                    imgLeftArrow.setVisibility(View.VISIBLE);
                }

            }else {

                CommonFunction.reWardDialogPop(SpecializeActivity.this,false,PreferenceHelper.getStringPreference(SpecializeActivity.this,TODAY_ATTEMPT_SPEC),"Your all today attempts have finished.\n Attempt Tomorrow.");

            }
        }else {

            CommonFunction.reWardDialogPop(SpecializeActivity.this,false,"Done","Today task has been completed !");

        }
        final StartAppAd  startAppAd = new StartAppAd(this);
        startAppAd.loadAd(StartAppAd.AdMode.REWARDED_VIDEO);
        imgRightArrow.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if (numberOfAttempt<50)
                {
                    numberOfAttempt++;
                    PreferenceHelper.setStringPreference(SpecializeActivity.this,TODAY_ATTEMPT_SPEC,String.valueOf(numberOfAttempt));

                    if (i<20)
                    {
                        isReplay=false;
                        //ShowAds();
                        startAppAd.showAd();
                        Counter();
                        isVideoCompleted=false;

                    }else {
                        imgRightArrow.setVisibility(View.INVISIBLE);
                    }

                }else {

                    CommonFunction.reWardDialogPop(SpecializeActivity.this,false,PreferenceHelper.getStringPreference(SpecializeActivity.this,TODAY_ATTEMPT_SPEC),"Your all today attempts have finished!");

                }




            }
        });

        imgLeftArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (numberOfAttempt<50)
                {
                    numberOfAttempt++;
                    PreferenceHelper.setStringPreference(SpecializeActivity.this,TODAY_ATTEMPT_SPEC,String.valueOf(numberOfAttempt));

                    startAppAd.showAd();
                    Counter();
                    isVideoCompleted=false;
                    isReplay=true;

                }else {
                    CommonFunction.reWardDialogPop(SpecializeActivity.this,true,PreferenceHelper.getStringPreference(SpecializeActivity.this,TODAY_ATTEMPT_SPEC),"Your all today attempts have finished!");
                }


            }
        });


    }


//    private void numberOfAdsActive()
//    {
//
////        InterstitialAd mInterstitialAd = new InterstitialAd(SpecializeActivity.this);
////        mInterstitialAd.setAdUnitId("ca-app-pub-2539068964968309/4828108518");
////        mInterstitialAd.loadAd(new AdRequest.Builder().build());
////
////
////        InterstitialAd mInterstitialAd2 = new InterstitialAd(SpecializeActivity.this);
////        mInterstitialAd2.setAdUnitId("ca-app-pub-3940256099942544/1033173712");
////        mInterstitialAd2.loadAd(new AdRequest.Builder().build());
////        mInterstitialAdList.add(mInterstitialAd2);
//////        mInterstitialAdList.add(mInterstitialAd);
//
//        mDatabase.child("interstitial_video").addValueEventListener(new ValueEventListener() {
//            @Override
//            public void onDataChange(DataSnapshot dataSnapshot) {
//
//                mInterstitialAdList.clear();
//
//                for (DataSnapshot snapshot: dataSnapshot.getChildren())
//                {
//                    if (snapshot.getValue(String.class).equalsIgnoreCase("yes"))
//                    {
//                        InterstitialAd mInterstitialAd = new InterstitialAd(SpecializeActivity.this);
//                        mInterstitialAd.setAdUnitId(GLOBAR_ADS_UNIT + snapshot.getKey());
//                        mInterstitialAd.loadAd(new AdRequest.Builder().build());
//                        mInterstitialAdList.add(mInterstitialAd);
//
//                    }
//
//                }
//            }
//
//            @Override
//            public void onCancelled(DatabaseError databaseError) {
//                // Getting Post failed, log a message
//                Log.w("Tag", "loadPost:onCancelled", databaseError.toException());
//
//            }
//        });
//
//    }

//    private void ShowAds()
//    {
//
//        if (mInterstitialAdList.size()>0)
//        {
//            final int caseAds=new Random().nextInt(mInterstitialAdList.size());
//
//            if (mInterstitialAdList.get(caseAds).isLoaded())
//            {
//
//                Counter();
//                mInterstitialAdList.get(caseAds).show();
//                mInterstitialAdList.get(caseAds).setAdListener(new AdListener() {
//                    @Override
//                    public void onAdLoaded() {
//
//                        Log.e("TAG","Loaded");
//
//                        // Code to be executed when an ad finishes loading.
//                    }
//
//                    @Override
//                    public void onAdFailedToLoad(int errorCode) {
//                        if (errorCode==3)
//                        {
//                            Toast.makeText(SpecializeActivity.this, "No Ads available right now!", Toast.LENGTH_SHORT).show();
//                        }else if (errorCode==2)
//                        {
//                            Toast.makeText(SpecializeActivity.this, "Network error right now!", Toast.LENGTH_SHORT).show();
//
//                        }else {
//
//                            Toast.makeText(SpecializeActivity.this, "Internal error right now!", Toast.LENGTH_SHORT).show();
//
//                        }
//                        Log.e("error_video",""+i);
//                    }
//
//                    @Override
//                    public void onAdOpened() {
//                        // Code to be executed when the ad is displayed.
//                        isVideoCompleted=false;
//                        Toast.makeText(SpecializeActivity.this, "Start Ads", Toast.LENGTH_SHORT).show();
//                    }
//
//                    @Override
//                    public void onAdLeftApplication() {
//                        // Code to be executed when the user has left the app.
//                    }
//
//                    @Override
//                    public void onAdClosed() {
//
//                        mInterstitialAdList.get(caseAds).loadAd(new AdRequest.Builder().build());
//
//                        // Code to be executed when when the interstitial ad is closed.
//                        isVideoCompleted=true;
//
//                        if (isVideoCompleted && isCounterFinished) {
//
//                            txtCountDown.setText("Play next ad video or replay.");
//                            txtCountDown.setVisibility(View.VISIBLE);
//                            lytCount.setVisibility(View.GONE);
//                            txtCountDown.setTextColor(getResources().getColor(R.color.green_500));
//
//                            if (i<15)
//                            {
//                                imgRightArrow.setVisibility(View.VISIBLE);
//                            }else {
//                                imgRightArrow.setVisibility(View.INVISIBLE);
//                            }
//
//                            imgLeftArrow.setVisibility(View.VISIBLE);
//
//                        }else {
//
//                            txtCountDown.setText("Ad video is not completed. Please play again !");
//                            txtCountDown.setVisibility(View.VISIBLE);
//                            lytCount.setVisibility(View.GONE);
//                            txtCountDown.setTextColor(getResources().getColor(R.color.colorAccent));
//                            imgRightArrow.setVisibility(View.INVISIBLE);
//                            imgLeftArrow.setVisibility(View.VISIBLE);
//                        }
//
//
//                    }
//                });
//            }else {
//
//                Toast.makeText(this, "Kindly wait for Ads load!", Toast.LENGTH_SHORT).show();
//
//            }
//
//
//        }else {
//
//            Toast.makeText(this, "No Active Ads Right Now !", Toast.LENGTH_SHORT).show();
//        }
//
//    }

    private void Counter()
    {
        // This function for Timer

        countDownTimer = new CountDownTimer(60000, 1000) {

            public void onTick(long millisUntilFinished) {

                isCounterFinished=false;

                txtCountDown.setText("Task is on going: ");
                txtCountDown.setTextColor(getResources().getColor(R.color.yellow_500));
                lytCount.setVisibility(View.VISIBLE);
                txtCountCircleDown.setText(String.valueOf(millisUntilFinished / 1000));
                imgRightArrow.setVisibility(View.INVISIBLE);
                imgLeftArrow.setVisibility(View.VISIBLE);

                //   progressDialog.show();
            }

            public void onFinish() {

                isCounterFinished=true;
                isVideoCompleted=true;
                if (isVideoCompleted && isCounterFinished) {

                    txtCountDown.setText("Play next ad video or replay.");
                    txtCountDown.setVisibility(View.VISIBLE);
                    lytCount.setVisibility(View.GONE);
                    txtCountDown.setTextColor(getResources().getColor(R.color.green_500));

                    if (i<20)
                    {
                        imgRightArrow.setVisibility(View.VISIBLE);

                    }else {

                        imgRightArrow.setVisibility(View.INVISIBLE);
                    }
                    imgLeftArrow.setVisibility(View.VISIBLE);

                }else {

                    txtCountDown.setText("Ad video is not completed. Please play again !");
                    txtCountDown.setVisibility(View.VISIBLE);
                    lytCount.setVisibility(View.GONE);
                    txtCountDown.setTextColor(getResources().getColor(R.color.colorAccent));
                    imgRightArrow.setVisibility(View.INVISIBLE);
                    imgLeftArrow.setVisibility(View.VISIBLE);
                }


                if (!isReplay)
                {
                    if (i<19)
                    {
                        i=i+1;
                        txtOutOFDown.setText(i+"/"+20);
                        PreferenceHelper.setStringPreference(SpecializeActivity.this,SPECL_COUNT_DOWN,""+i);
                        PreferenceHelper.setStringPreference(SpecializeActivity.this,SPEC_SESSION_DATE,getCurrentDate());

                    }else {

                        i=i+1;
                        txtOutOFDown.setText("Done");
                        txtCountDown.setText("Today task has been completed !");
                        imgRightArrow.setVisibility(View.INVISIBLE);

                        PreferenceHelper.isFlag(SpecializeActivity.this,SPEC_REWARDSESSION,true);
                        PreferenceHelper.setStringPreference(SpecializeActivity.this,SPECL_COUNT_DOWN,""+i);
                        PreferenceHelper.setStringPreference(SpecializeActivity.this,SPEC_SESSION_DATE,getCurrentDate());

                        CommonFunction.reWardDialogPop(SpecializeActivity.this,true,PreferenceHelper.getStringPreference(SpecializeActivity.this,SPECL_COUNT_DOWN),"Your task has been completed !");

                        specilizeMood(String.valueOf(20));

                    }
                }



            }
        }.start();
    }

    private void bannerAdLoad()
    {
        mDatabase.child("banner_for_interstitial/2602234579").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                if (dataSnapshot.getValue(String.class).equalsIgnoreCase("yes"))
                {
//                    mAdView.setVisibility(View.VISIBLE);
//                    AdRequest adRequest = new AdRequest.Builder().build();
//                    mAdView.loadAd(adRequest);

                }else {
//                    mAdView.setVisibility(View.INVISIBLE);
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
    //////////////////reWardAPI /////////////////////////////////////////////

    private void specilizeMood(final String userCast)
    {
        progressDialog=new ProgressDialog(this);
        progressDialog.setMessage("Loading...");
        progressDialog.show();
        progressDialog.setCancelable(false);
        final String m_androidId = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);

        Log.e("TAG",m_androidId);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Configs.url_SPECIAL_VIDEO, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {


                try {
                    progressDialog.dismiss();
                    Log.e("forgetResponse0",response);
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.optString("error").equalsIgnoreCase("false"))
                    {
                        Toast.makeText(SpecializeActivity.this, "Moon has Uploaded", Toast.LENGTH_SHORT).show();
                    }else
                    {
                        Toast.makeText(SpecializeActivity.this, "Moon has not Uploaded", Toast.LENGTH_SHORT).show();

                    }

                }catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        },new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Toast.makeText(SpecializeActivity.this,"Some error occurred",Toast.LENGTH_SHORT).show();

            }
        }){
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put(KEY_USER_ID, _userId);
                params.put(KEY_CLIENT_ID, clientId(SpecializeActivity.this));
                params.put(USER_CAST, userCast);
                params.put(ANDROID_ID, m_androidId);

                return params;
            }

        };
        VolleySingleton.getInstance(SpecializeActivity.this).addToRequestQueue(stringRequest);

    }


}
