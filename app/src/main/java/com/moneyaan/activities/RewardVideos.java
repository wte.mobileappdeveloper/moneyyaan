package com.moneyaan.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.CountDownTimer;
import android.provider.Settings;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.moneyaan.ApiHandler.VolleySingleton;
import com.moneyaan.CommonFunction;
import com.moneyaan.LocalSaved.PreferenceHelper;
import com.moneyaan.R;
import com.moneyaan.configs.Configs;
import com.moneyaan.utils.InternetConnectivity.BaseActivity;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.moneyaan.CommonFunction.md5;
import static com.moneyaan.configs.Configs.KEY_CLIENT_ID;
import static com.moneyaan.configs.Configs.clientId;
import static com.moneyaan.utils.Constant.COUNT_DOWN;
import static com.moneyaan.utils.Constant.REWARDSESSION;
import static com.moneyaan.utils.Constant.USER_CAST;
import static com.moneyaan.utils.Constant.WEB_THE_DIRT_SHEET;

public class RewardVideos extends BaseActivity {
    public static final String KEY_USER_ID = "customer_id";
    private Toolbar toolbar;
    private ImageView imgRightArrow,imgLeftArrow;
    public  CountDownTimer countDownTimer;
    private TextView txtCountDown,txtOutOFDown,txtCountCircleDown;
    Integer i=0;
    private RelativeLayout lytCount;
    private WebView webTopNews;
    boolean isVideoCompleted=true;
    boolean isCounterFinished=true;
    boolean isReplay=true;
    ProgressDialog progressDoalog;
    String _userId;
    String deviceId;
    private ProgressBar webLoading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reward_videos);

        toolbar= findViewById(R.id.toggle_toolbar);
        TextView toolactName= findViewById(R.id.act_name);

        toolactName.setText("Reward Videos");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        imgRightArrow=findViewById(R.id.imgRightArrow);
        txtCountDown=findViewById(R.id.txtCountDown);
        txtOutOFDown=findViewById(R.id.txtOutOFDown);
        txtCountCircleDown=findViewById(R.id.txtCountCircleDown);
        imgLeftArrow=findViewById(R.id.imgLeftArrow);
        lytCount=findViewById(R.id.lytCount);

        webLoading=findViewById(R.id.webLoading);


        // this is for admob


        webTopNews=findViewById(R.id.webTopNews);

        CommonFunction.startWebView(RewardVideos.this,webTopNews,WEB_THE_DIRT_SHEET,webLoading);


        SharedPreferences preferences = getSharedPreferences("login", Context.MODE_PRIVATE);
        _userId = preferences.getString("User_Id", "");


//        MobileAds.initialize(this, "ca-app-pub-2539068964968309~5985647677");

        // Use an activity context to get the rewarded video instance.


        String android_id = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);
        deviceId = md5(android_id).toUpperCase();


         i=Integer.parseInt(PreferenceHelper.getStringPreference(RewardVideos.this,COUNT_DOWN));
         txtOutOFDown.setText(PreferenceHelper.getStringPreference(RewardVideos.this,COUNT_DOWN)+"/"+5);


        if (PreferenceHelper.getStringPreference(RewardVideos.this,COUNT_DOWN).equals("5"))
        {
            txtOutOFDown.setText("Done");
            txtCountDown.setText("Today task has been completed !");
            txtCountDown.setTextColor(getResources().getColor(R.color.material_green));

        }else {

            txtOutOFDown.setText(PreferenceHelper.getStringPreference(RewardVideos.this,COUNT_DOWN)+"/"+5);
        }


        if (!PreferenceHelper.isGetFlag(RewardVideos.this,REWARDSESSION))
        {
            CommonFunction.reWardDialogPop(RewardVideos.this,false,PreferenceHelper.getStringPreference(RewardVideos.this,COUNT_DOWN)+"/5","Today task is incomplete !");

        }else {

            CommonFunction.reWardDialogPop(RewardVideos.this,false,"Done","Today task has been completed !");
            imgRightArrow.setVisibility(View.INVISIBLE);
            imgLeftArrow.setVisibility(View.VISIBLE);
        }

        //loadRewardedVideoAd();

//        imgRightArrow.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (i<5)
//                {
//                    if (mRewardedVideoAd.isLoaded())
//                    {
//                        isVideoCompleted=false;
//                        isReplay=false;
//                        mRewardedVideoAd.show();
//                        txtCountDown.setText("Task is on going: ");
//                        txtCountDown.setTextColor(getResources().getColor(R.color.yellow_500));
//
//                        Counter();
//
//                        imgRightArrow.setVisibility(View.INVISIBLE);
//                        imgLeftArrow.setVisibility(View.VISIBLE);
//
//                    }else {
//
//                        loadRewardedVideoAd();
//                    }
//                }else {
//                    imgRightArrow.setVisibility(View.INVISIBLE);
//                }
//
//            }
//        });

//        imgLeftArrow.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (mRewardedVideoAd.isLoaded())
//                {
//                    mRewardedVideoAd.show();
//                    isReplay=true;
//                    //  imgRightArrow.setVisibility(View.VISIBLE);
//
//                }
//            }
//        });


    }

    private void Counter()
    {
        // This function for Timer

        countDownTimer = new CountDownTimer(20000, 1000) {

            public void onTick(long millisUntilFinished) {


                isCounterFinished=false;
                lytCount.setVisibility(View.VISIBLE);
                txtCountCircleDown.setText(String.valueOf(millisUntilFinished / 1000));
                imgRightArrow.setVisibility(View.INVISIBLE);
                imgLeftArrow.setVisibility(View.VISIBLE);

                //   progressDialog.show();
            }

            public void onFinish() {

                isCounterFinished=true;

                if (isVideoCompleted && isCounterFinished)
                {

                    txtCountDown.setVisibility(View.VISIBLE);
                    lytCount.setVisibility(View.GONE);
                    txtCountDown.setTextColor(getResources().getColor(R.color.green_500));
                    imgLeftArrow.setVisibility(View.VISIBLE);

                    if (i<5)
                    {
                        txtCountDown.setText("Play next ad video or replay.");
                        imgRightArrow.setVisibility(View.VISIBLE);
                    }else {
                        txtOutOFDown.setText("Done");
                        txtCountDown.setText("Today task has been completed !");
                        imgRightArrow.setVisibility(View.INVISIBLE);
                    }


                }else {
                    txtCountDown.setText("Ad video is not completed. Please play again !");
                    txtCountDown.setVisibility(View.VISIBLE);
                    lytCount.setVisibility(View.GONE);
                    txtCountDown.setTextColor(getResources().getColor(R.color.colorAccent));
                    imgRightArrow.setVisibility(View.INVISIBLE);
                    imgLeftArrow.setVisibility(View.VISIBLE);
                }

            }
        }.start();
    }
    private void loadRewardedVideoAd() {


//        mRewardedVideoAd.loadAd(ADS_REWARD_UNITCODE,
//                new AdRequest.Builder().build());

        Log.e("DeviceID", "is Admob Test Device ? "+deviceId);
    }


//    @Override
//    public void onRewardedVideoAdLoaded() {
//
//        Log.e("error_video","");
//
//    }
//
//    @Override
//    public void onRewardedVideoAdOpened() {
//
//        Toast.makeText(this, "Ads Opens", Toast.LENGTH_SHORT).show();
//    }
//
//    @Override
//    public void onRewardedVideoStarted() {
//        Toast.makeText(this, "Reward Video Start ", Toast.LENGTH_SHORT).show();
//    }
//
//    @Override
//    public void onRewardedVideoAdClosed() {
//        loadRewardedVideoAd();
//        Toast.makeText(this, "Reward Video Closed !", Toast.LENGTH_SHORT).show();
//
//
//
//    }
//
//    @Override
//    public void onRewarded(RewardItem rewardItem) {
//
////        Toast.makeText(this, "onRewarded! currency: " + rewardItem.getType() + "  amount: " +
////                rewardItem.getAmount(), Toast.LENGTH_SHORT).show();
//
//        Toast.makeText(this, "onRewarded! currency: " + rewardItem.getType() + "  Mood: " +
//                1, Toast.LENGTH_SHORT).show();
//
//
//        txtCountDown.setText("Task is on going: ");
//        txtCountDown.setTextColor(getResources().getColor(R.color.yellow_500));
//        if (i<5)
//        {
//            imgRightArrow.setVisibility(View.VISIBLE);
//        }else {
//            imgRightArrow.setVisibility(View.GONE);
//        }
//
//    }
//
//    @Override
//    public void onRewardedVideoAdLeftApplication() {
//
//        txtCountDown.setText("Incomplete !");
//    }
//
//    @Override
//    public void onRewardedVideoAdFailedToLoad(int errorCode) {
//
//        if (errorCode==3)
//        {
//            Toast.makeText(this, "No Ads available right now!", Toast.LENGTH_SHORT).show();
//        }else if (errorCode==2)
//        {
//            Toast.makeText(this, "Network error right now!", Toast.LENGTH_SHORT).show();
//
//        }else {
//
//            Toast.makeText(this, "Internal error right now!", Toast.LENGTH_SHORT).show();
//
//        }
//        Log.e("error_video",""+errorCode);
//
//    }
//
//    @Override
//    public void onRewardedVideoCompleted() {
//
//
//        Toast.makeText(this, "Complete Video ", Toast.LENGTH_SHORT).show();
//
//        isVideoCompleted=true;
//        txtCountDown.setText("Completed");
//        txtCountDown.setTextColor(getResources().getColor(R.color.material_green));
//
//        if (!isReplay)
//        {
//            if (i<4)
//            {
//                i=i+1;
//                txtOutOFDown.setText(i+"/"+5);
//                PreferenceHelper.setStringPreference(RewardVideos.this,COUNT_DOWN,""+i);
//                PreferenceHelper.setStringPreference(RewardVideos.this,SESSION_DATE,getCurrentDate());
//
//
//            }else {
//
//                i=i+1;
//                Log.e("Currentdate",getCurrentDate());
//                txtOutOFDown.setText("Done");
//                txtCountDown.setText("Complete Your Task !");
//                imgRightArrow.setVisibility(View.INVISIBLE);
//                PreferenceHelper.isFlag(RewardVideos.this,REWARDSESSION,true);
//                PreferenceHelper.setStringPreference(RewardVideos.this,COUNT_DOWN,""+i);
//                PreferenceHelper.setStringPreference(RewardVideos.this,SESSION_DATE,getCurrentDate());
//
//                CommonFunction.reWardDialogPop(RewardVideos.this,false,PreferenceHelper.getStringPreference(RewardVideos.this,COUNT_DOWN),"Your task has completed !");
//
//                reWardVideo(String.valueOf(5));
//            }
//        }
//
//
//
//    }


    @Override
    public void onResume() {


        super.onResume();
    }

    @Override
    public void onPause() {

        super.onPause();
    }

    @Override
    public void onDestroy() {

        super.onDestroy();
    }


    //////////////////reWardAPI /////////////////////////////////////////////
    private void reWardVideo(final String userCast)
    {
        progressDoalog=new ProgressDialog(this);
        progressDoalog.setMessage("Loading...");
        progressDoalog.show();
        progressDoalog.setCancelable(true);


        StringRequest stringRequest = new StringRequest(Request.Method.POST, Configs.url_RewardAPI, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {


                try {
                    progressDoalog.dismiss();
                    Log.e("forgetResponse0",response);
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.optString("error").equalsIgnoreCase("false"))
                    {
                        Toast.makeText(RewardVideos.this, "Mood has Uploaded", Toast.LENGTH_SHORT).show();
                    }else
                    {
                        Toast.makeText(RewardVideos.this, "Mood has not Uploaded", Toast.LENGTH_SHORT).show();

                    }

                }catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        },new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Toast.makeText(RewardVideos.this,"Some error occurred",Toast.LENGTH_SHORT).show();

            }
        }){
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put(KEY_USER_ID, _userId);
                params.put(KEY_CLIENT_ID, clientId(RewardVideos.this));
                params.put(USER_CAST, userCast);

                return params;
            }

        };
        VolleySingleton.getInstance(RewardVideos.this).addToRequestQueue(stringRequest);


    }
}
