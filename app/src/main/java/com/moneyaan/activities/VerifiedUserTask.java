package com.moneyaan.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.CountDownTimer;
import android.support.v4.widget.SwipeRefreshLayout;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.moneyaan.ApiHandler.VolleySingleton;
import com.moneyaan.R;
import com.moneyaan.configs.Configs;
import com.moneyaan.utils.InternetConnectivity.BaseActivity;
import com.startapp.android.publish.adsCommon.StartAppAd;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.moneyaan.configs.Configs.KEY_CLIENT_ID;
import static com.moneyaan.configs.Configs.clientId;
import static com.moneyaan.utils.Constant.ADS_REWARD_UNITCODE;
import static com.moneyaan.utils.Constant.STATUS_VERIFY;

public class VerifiedUserTask extends BaseActivity {
    public static final String KEY_USER_ID = "customer_id";
    LinearLayout lytSubscribes,lytWatchRewadTask,lytDoAllTask;
    ProgressDialog progressDoalog;
    String _userId;
    TextView txtDoAlltask,txtSecondSetup,txtSubscribes,txtCountCircleDown,txtSubscribCountCircleDown;
    Button btnLetStart;
    SharedPreferences myPrefsForSesssion;
    private  CountDownTimer countDownTimer;
    private RelativeLayout lytCount,lytSubscribCount;
    boolean isVideoCompleted=true;
    boolean isCounterFinished=true;

    private SwipeRefreshLayout swifeRefresh;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verified_user_task);

        initView();

    }

    private void initView()
    {
        lytSubscribes=findViewById(R.id.lytSubscribes);
        lytWatchRewadTask=findViewById(R.id.lytWatchRewadTask);
        lytDoAllTask=findViewById(R.id.lytDoAllTask);
        txtSecondSetup=findViewById(R.id.txtSecondSetup);
        txtSubscribes=findViewById(R.id.txtSubscribes);
        txtDoAlltask=findViewById(R.id.txtDoAlltask);
        txtCountCircleDown=findViewById(R.id.txtCountCircleDown);
        lytCount=findViewById(R.id.lytCount);
        txtSubscribCountCircleDown=findViewById(R.id.txtSubscribCountCircleDown);
        lytSubscribCount=findViewById(R.id.lytSubscribCount);
        btnLetStart=findViewById(R.id.btnLetStart);
        swifeRefresh=findViewById(R.id.swifeRefresh);
        lytWatchRewadTask.setEnabled(false);
        lytSubscribes.setEnabled(false);

        SharedPreferences preferences = this.getApplicationContext().getSharedPreferences("login", Context.MODE_PRIVATE);
        _userId = preferences.getString("User_Id", "");
        Log.e("_userId",_userId);
        lytSubscribes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                CounterForSubscription();
                startActivity(new Intent(Intent.ACTION_VIEW,   Uri.parse("https://www.youtube.com/channel/UCHUuRLLdEQdXBVk161kJgKA")));
            }
        });

        final StartAppAd startAppAd = new StartAppAd(this);
        startAppAd.loadAd(StartAppAd.AdMode.REWARDED_VIDEO);

        lytWatchRewadTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startAppAd.showAd();
                isVideoCompleted=true;
                Counter();
            }
        });

        lytDoAllTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(VerifiedUserTask.this,DoAllTaskAcitivity.class));
            }
        });

        btnLetStart.setEnabled(false);
        btnLetStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                myPrefsForSesssion = getSharedPreferences("session", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor1 = myPrefsForSesssion.edit();
                editor1.putBoolean("remember", true);
                editor1.putBoolean("verifyUser",true);
                editor1.apply();
                startActivity(new Intent(VerifiedUserTask.this,Home.class));
                finishAffinity();
            }
        });

        swifeRefresh.setColorSchemeColors(Color.RED, Color.GREEN, Color.BLUE, Color.YELLOW, Color.MAGENTA);

        swifeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swifeRefresh.setRefreshing(true);

                verifyAllStepsStatus();
            }
        });


    }

    @Override
    protected void onResume() {
        super.onResume();
        verifyAllStepsStatus();
    }

    private void Counter()
    {
        // This function for Timer

        countDownTimer = new CountDownTimer(60000, 1000) {

            public void onTick(long millisUntilFinished) {


                lytWatchRewadTask.setEnabled(false);
                lytCount.setVisibility(View.VISIBLE);
                txtCountCircleDown.setText(String.valueOf(millisUntilFinished / 1000));


                //   progressDialog.show();
            }

            public void onFinish() {

                isCounterFinished=true;
                lytWatchRewadTask.setEnabled(true);
                if (isVideoCompleted && isCounterFinished)
                {
                    lytCount.setVisibility(View.GONE);
                    verifySecondStepsStatus();

                }else {
                    txtCountCircleDown.setVisibility(View.VISIBLE);
                    lytCount.setVisibility(View.GONE);
                    txtCountCircleDown.setTextColor(getResources().getColor(R.color.colorAccent));

                }

            }
        }.start();
    }

    private void CounterForSubscription()
    {
        // This function for Timer

        countDownTimer = new CountDownTimer(60000, 1000) {

            public void onTick(long millisUntilFinished) {

                lytSubscribCount.setVisibility(View.VISIBLE);
                txtSubscribCountCircleDown.setText(String.valueOf(millisUntilFinished / 1000));
                lytSubscribes.setEnabled(false);

                //   progressDialog.show();
            }

            public void onFinish() {

                lytSubscribCount.setVisibility(View.GONE);
                verifyThirdStepsStatus();
            }
        }.start();
    }


    //.................. verifySteps .......................//
    private void verifyAllStepsStatus()
    {
        progressDoalog=new ProgressDialog(this);
        progressDoalog.setMessage("Loading...");
        progressDoalog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Configs.url_verify_step, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    progressDoalog.dismiss();
                    swifeRefresh.setRefreshing(false);
                    Log.e("forgetResponse0",response);
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.optString("error").equalsIgnoreCase("false"))
                    {

                        if (jsonObject.optString("verify_step1").equalsIgnoreCase("Y"))
                        {
                            txtDoAlltask.setText("Step first done !");
                            txtDoAlltask.setTextColor(getResources().getColor(R.color.white));
                            lytDoAllTask.setBackgroundColor(getResources().getColor(R.color.trans_green));
                            lytDoAllTask.setEnabled(false);

                            lytWatchRewadTask.setEnabled(true);
                            lytWatchRewadTask.setBackgroundColor(getResources().getColor(R.color.dot_dark_screen1));
                        }

                        if (jsonObject.optString("verify_step2").equalsIgnoreCase("Y"))
                        {
                            txtSecondSetup.setText("Step second done !");
                            txtSecondSetup.setTextColor(getResources().getColor(R.color.white));
                            lytWatchRewadTask.setBackgroundColor(getResources().getColor(R.color.trans_green));
                            lytWatchRewadTask.setEnabled(false);
                            lytCount.setVisibility(View.INVISIBLE);
                            lytSubscribes.setEnabled(true);
                            lytSubscribes.setBackgroundColor(getResources().getColor(R.color.dot_dark_screen1));

                        }

                        if (jsonObject.optString("verify_step3").equalsIgnoreCase("Y"))
                        {
                            txtSubscribes.setText("Step third done !");
                            txtSubscribes.setTextColor(getResources().getColor(R.color.white));
                            lytSubscribes.setBackgroundColor(getResources().getColor(R.color.trans_green));
                            lytSubscribes.setEnabled(false);
                            lytSubscribCount.setVisibility(View.INVISIBLE);
                            //  lytSubscribCount.setVisibility(View.INVISIBLE);

                        }

                        if (jsonObject.optString("verify_step1").equalsIgnoreCase("Y") && jsonObject.optString("verify_step2").equalsIgnoreCase("Y") && jsonObject.optString("verify_step3").equalsIgnoreCase("Y"))
                        {
                            btnLetStart.setEnabled(true);
                            btnLetStart.setTextColor(getResources().getColor(R.color.white));
                            btnLetStart.setBackgroundColor(getResources().getColor(R.color.green_500));
                        }



                    }else
                    {

                    }

                }catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        },new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Toast.makeText(VerifiedUserTask.this,"Some error occurred",Toast.LENGTH_SHORT).show();

            }
        }){
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put(KEY_USER_ID, _userId);
                params.put(KEY_CLIENT_ID, clientId(VerifiedUserTask.this));

                return params;
            }

        };
        VolleySingleton.getInstance(VerifiedUserTask.this).addToRequestQueue(stringRequest);


    }

    //.................. verifySecondSteps .......................//
    private void verifySecondStepsStatus()
    {
        progressDoalog=new ProgressDialog(this);
        progressDoalog.setMessage("Loading...");
        progressDoalog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Configs.url_verify_stepsecondstatus, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    progressDoalog.dismiss();
                    Log.e("forgetResponse0",response);
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.optString("error").equalsIgnoreCase("false"))
                    {

                        txtSecondSetup.setText("Step second done !");
                        txtSecondSetup.setTextColor(getResources().getColor(R.color.white));
                        lytWatchRewadTask.setBackgroundColor(getResources().getColor(R.color.trans_green));
                        verifyAllStepsStatus();


                    }else
                    {

                    }

                }catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        },new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Toast.makeText(VerifiedUserTask.this,"Some error occurred",Toast.LENGTH_SHORT).show();

            }
        }){
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put(KEY_USER_ID, _userId);
                params.put(KEY_CLIENT_ID, clientId(VerifiedUserTask.this));
                params.put(STATUS_VERIFY, "Y");


                return params;
            }

        };
        VolleySingleton.getInstance(VerifiedUserTask.this).addToRequestQueue(stringRequest);


    }

    //.................. verifyThirdSteps .......................//
    private void verifyThirdStepsStatus()
    {
        progressDoalog=new ProgressDialog(this);
        progressDoalog.setMessage("Loading...");
        progressDoalog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Configs.url_verify_stepthirdstatus, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    progressDoalog.dismiss();

                    Log.e("forgetResponse0",response);
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.optString("error").equalsIgnoreCase("false"))
                    {

                        txtSubscribes.setText("Step third done !");
                        txtSubscribes.setTextColor(getResources().getColor(R.color.white));
                        lytSubscribes.setBackgroundColor(getResources().getColor(R.color.trans_green));
                        verifyAllStepsStatus();


                    }else
                    {
                        lytSubscribes.setEnabled(true);
                    }

                }catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        },new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Toast.makeText(VerifiedUserTask.this,"Some error occurred",Toast.LENGTH_SHORT).show();

            }
        }){
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put(KEY_USER_ID, _userId);
                params.put(KEY_CLIENT_ID, clientId(VerifiedUserTask.this));
                params.put(STATUS_VERIFY, "Y");

                return params;
            }

        };
        VolleySingleton.getInstance(VerifiedUserTask.this).addToRequestQueue(stringRequest);


    }


}
