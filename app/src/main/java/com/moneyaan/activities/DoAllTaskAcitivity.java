package com.moneyaan.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.widget.SwipeRefreshLayout;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.moneyaan.R;
import com.moneyaan.configs.Configs;
import com.moneyaan.models.TodayTaskModel;
import com.moneyaan.recyclerAdapter.HomeRecyclerAdapter_ToDoTasks;
import com.moneyaan.utils.InternetConnectivity.BaseActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import static com.moneyaan.activities.SpamActivity.KEY_USER_ID;
import static com.moneyaan.configs.Configs.KEY_CLIENT_ID;
import static com.moneyaan.configs.Configs.clientId;

public class DoAllTaskAcitivity extends BaseActivity {

    SwipeRefreshLayout mSwipeRefreshLayout;
    ProgressDialog progressDialog;
    LinearLayout txt_no_task_available;
    RecyclerView doAllTAskRecycle;
    public static final String KEY_CUSTMRID = "customer_id";
    String _userId;
    private Toolbar toolbar;
    ArrayList<TodayTaskModel> list = new ArrayList<>();
    HomeRecyclerAdapter_ToDoTasks globalAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_do_all_task_acitivity);

        toolbar= findViewById(R.id.toggle_toolbar);
        TextView toolactName= findViewById(R.id.act_name);
        toolactName.setText("Do All Task");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        mSwipeRefreshLayout=findViewById(R.id.mSwipeRefreshLayout);
        doAllTAskRecycle=findViewById(R.id.doAllTAskRecycle);
        progressDialog=new ProgressDialog(this);
        txt_no_task_available=findViewById(R.id.txt_no_task_available);

        SharedPreferences preferences = getSharedPreferences("login", Context.MODE_PRIVATE);
        _userId = preferences.getString("User_Id", "");

        globalAdapter= new HomeRecyclerAdapter_ToDoTasks(DoAllTaskAcitivity.this,list);
        doAllTAskRecycle.setAdapter(globalAdapter);
        doAllTAskRecycle.setLayoutManager(new GridLayoutManager(DoAllTaskAcitivity.this, 2));
        doAllTAskRecycle.setHasFixedSize(true);

        mSwipeRefreshLayout.setColorSchemeColors(Color.RED, Color.CYAN, Color.BLUE, Color.YELLOW, Color.MAGENTA);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mSwipeRefreshLayout.setRefreshing(true);

                assignNewTask();
            }
        });
        assignNewTask();



    }

    //-----------------------------assign new task aPI---------------------------------
    private void assignNewTask() {
        progressDialog.show();



        StringRequest stringRequest = new StringRequest(Request.Method.POST, Configs.url_newTaskAssign,
                new Response.Listener<String>() {
                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                    @Override
                    public void onResponse(String response) {
                        Log.e("Frag_tdyTsk assgn resp",response);

                        doAllTasks();


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            doAllTasks();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put(KEY_CUSTMRID, _userId);
                params.put(KEY_CLIENT_ID, clientId(DoAllTaskAcitivity.this));
                return params;
            }

        };

        RequestQueue requestQueue = Volley.newRequestQueue(DoAllTaskAcitivity.this);
        requestQueue.add(stringRequest);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

    }


    //------------------------------------fetch todays task------------------------------
    private void doAllTasks() {

        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Configs.url_workAssign,
                new Response.Listener<String>() {
                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                    @Override
                    public void onResponse(String response) {
                        Log.e("Frag_todayTsk resp",response);

                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            if (jsonObject.optString("error").equalsIgnoreCase("true") || jsonObject.optString("error_msg").equalsIgnoreCase("No Task Avaialable !!")) {
                                list.clear();
                                txt_no_task_available.setVisibility(View.VISIBLE);

                            } else if (jsonObject.optString("error").equalsIgnoreCase("false") || jsonObject.optString("error_msg").equalsIgnoreCase("Task List Available")) {
                                txt_no_task_available.setVisibility(View.GONE);
                                JSONArray jsonArray = jsonObject.getJSONArray("data");

                                list.clear();

                                for (int i = 0; i < jsonArray.length(); i++) {

                                    TodayTaskModel todayTask = new TodayTaskModel();

                                    JSONObject jsonObj = jsonArray.getJSONObject(i);

// For Set data

                                    todayTask.setAmount_Credit(jsonObj.getString("pkgcost"));//
                                    todayTask.setPackage_id(jsonObj.getString("campaign_id"));//
                                    todayTask.setPackage_Name(jsonObj.getString("camping_name"));//
//                                    todayTask.setWorkassign_Date(jsonObj.getString("assign_work_date"));//
                                    todayTask.setWork_url(jsonObj.getString("com_url"));//
                                    todayTask.setWebUrl(jsonObj.getString("webUrl"));//
                                    todayTask.setAppDesc(jsonObj.getString("com_desc"));//
                                    todayTask.setAppSdesc(jsonObj.getString("com_sdesc"));
                                    todayTask.setAppDesc_hindi(jsonObj.getString("cam_desc_in_hindi"));//
                                    todayTask.setAppSdesc_hindi(jsonObj.getString("cam_short_desc_in_hindi"));
                                    todayTask.setWebUrl_hindi(jsonObj.getString("webUrl_in_hindi"));//
                                    todayTask.setPause(jsonObj.getString("pous"));//               used in capping lim
                                    todayTask.setWorkStatus(jsonObj.getString("assing_work_status"));//
                                    todayTask.setClickBLabel(jsonObj.getString("button_text"));
                                    todayTask.setIcon(jsonObj.getString("icone_url"));

                                    if (jsonObj.getString("verifynew_user").equalsIgnoreCase("Y"))
                                    {
                                        list.add(todayTask);
                                    }


                                }

                            }


                            globalAdapter.notifyDataSetChanged();
                            int gidSize=list.size()/2+list.size()%2;

                            try {

                                doAllTAskRecycle.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, dpToPx(155)*gidSize));

                            }catch (Exception e){
                                doAllTAskRecycle.setNestedScrollingEnabled(true);
                                doAllTAskRecycle.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

                            }

                        } catch (JSONException e) {
                            Log.e("frag_tda error",e.toString());
                            e.printStackTrace();

                        }

                        fetchInProgress();

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mSwipeRefreshLayout.setRefreshing(false);
                txt_no_task_available.setVisibility(View.VISIBLE);
                Toast.makeText(DoAllTaskAcitivity.this, "Server is not Responding !!", Toast.LENGTH_SHORT).show();

                progressDialog.dismiss();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put(KEY_CUSTMRID, _userId);
                params.put(KEY_CLIENT_ID, clientId(DoAllTaskAcitivity.this));
                Log.e("Frag_todayTsk parms",params.toString());
                return params;
            }

        };

        RequestQueue requestQueue = Volley.newRequestQueue(Objects.requireNonNull(DoAllTaskAcitivity.this));
        requestQueue.add(stringRequest);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

    }



    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public int dpToPx(int dp) {
        float density = Objects.requireNonNull(DoAllTaskAcitivity.this).getResources()
                .getDisplayMetrics()
                .density;
        return Math.round((float) dp * density);
    }




    private void fetchInProgress() {


        StringRequest stringRequest = new StringRequest(Request.Method.POST, Configs.url_inProgress,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("Frag_inprogress resp",response);
                        mSwipeRefreshLayout.setRefreshing(false);
                        progressDialog.dismiss();

                        try {

                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.optString("error").equalsIgnoreCase("true")) {

                                txt_no_task_available.setVisibility(View.VISIBLE);
                            } else if (jsonObject.optString("error").equalsIgnoreCase("false")) {
                                txt_no_task_available.setVisibility(View.GONE);

                                JSONArray jsonArray = jsonObject.getJSONArray("data");
                                for (int i = 0; i < jsonArray.length(); i++) {

                                    TodayTaskModel todayTask = new TodayTaskModel();
                                    JSONObject jsonObj = jsonArray.getJSONObject(i);
// For Set data
                                    todayTask.setAppDesc_hindi(jsonObj.getString("cam_desc_in_hindi"));//
                                    todayTask.setAppSdesc_hindi(jsonObj.getString("cam_short_desc_in_hindi"));
                                    todayTask.setWebUrl_hindi(jsonObj.getString("webUrl_in_hindi"));//

                                    todayTask.setAmount_Credit(jsonObj.getString("pkgcost"));//
                                    todayTask.setPackage_id(jsonObj.getString("campaign_id"));//
                                    todayTask.setPackage_Name(jsonObj.getString("camping_name"));//
//                                        todayTask.setWorkassign_Date(jsonObj.getString("assign_work_date"));//
                                    todayTask.setWebUrl(jsonObj.getString("webUrl"));//
                                    todayTask.setWork_url(jsonObj.getString("com_url"));//
                                    todayTask.setAppDesc(jsonObj.getString("com_desc"));//
                                    todayTask.setWorkStatus(jsonObj.getString("assing_work_status"));//
                                    todayTask.setClickBLabel("Complete");
                                    todayTask.setIcon(jsonObj.getString("icone_url"));
                                    todayTask.setAppSdesc(jsonObj.getString("com_sdesc"));
                                    todayTask.setPause(jsonObj.getString("pous"));//

                                    if (jsonObj.getString("verifynew_user").equalsIgnoreCase("Y"))
                                    {
                                        list.add(todayTask);
                                    }

                                }

                            }

                            globalAdapter.notifyDataSetChanged();

                            mSwipeRefreshLayout.setRefreshing(false);

                        } catch (JSONException e) {

                            Log.e("error in progress",e.toString());
                            e.printStackTrace();
                        }

                        mSwipeRefreshLayout.setRefreshing(false);

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        txt_no_task_available.setVisibility(View.VISIBLE);
                        Toast.makeText(DoAllTaskAcitivity.this, "Server is Not Responding !!", Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put(KEY_CLIENT_ID, clientId(DoAllTaskAcitivity.this));
                params.put(KEY_USER_ID,_userId);
                return params;
            }

        };

        RequestQueue requestQueue = Volley.newRequestQueue(DoAllTaskAcitivity.this);
        requestQueue.add(stringRequest);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

    }
}
