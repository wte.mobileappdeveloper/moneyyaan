package com.moneyaan.activities;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.CountDownTimer;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.moneyaan.R;
import com.moneyaan.models.QuizModel;
import com.moneyaan.utils.InternetConnectivity.BaseActivity;
import com.startapp.android.publish.adsCommon.StartAppAd;

import java.util.ArrayList;
import java.util.Collections;

public class Quiz extends BaseActivity{
    ImageView questionImage;
    TextView questionNumber,question,timeRemaining,next,status;
    Button answer1,answer2,answer3,answer4;
    int questioncount=1;
    CountDownTimer countDownTimer;
    Dialog dialog;
    Toolbar toolbar;
    int correctcount=0;
    int missedcount=0;
    boolean doubleBackToExitPressedOnce = false;
    AlertDialog alertDialog;
    TextView toolUname;
    ArrayList<QuizModel> quizlist=new ArrayList<>();

    ArrayList<String> questions=new ArrayList<>();
    ArrayList<String> ans1=new ArrayList<>();
    ArrayList<String> ans2=new ArrayList<>();
    ArrayList<String> ans3=new ArrayList<>();
    ArrayList<String> ans4=new ArrayList<>();
    ArrayList<String> correctans=new ArrayList<>();

    QuizModel quizModel;
    int correctAnswer=1;
    StartAppAd startAppAd;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz);
        timeRemaining=findViewById(R.id.timeremaining);
//        questionImage=findViewById(R.id.question_image);
        question=findViewById(R.id.question);
        questionNumber=findViewById(R.id.question_number);
        answer1=findViewById(R.id.answer1);
        answer2=findViewById(R.id.answer2);
        answer3=findViewById(R.id.answer3);
        answer4=findViewById(R.id.answer4);


        startAppAd = new StartAppAd(this);

        getQuestions();


        ////////////////toolbaar////////////////////

        toolbar= (Toolbar) findViewById(R.id.toggle_toolbar);
        TextView toolactName= findViewById(R.id.act_name);
        toolactName.setText("Take a Quiz");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        //////////////initiate dialog box and addView////////////////

         dialog= new Dialog(Quiz.this,R.style.floatingDialog);
        dialog.setContentView(R.layout.dialog_quiz);
        next =  dialog.findViewById(R.id.next);
        status=dialog.findViewById(R.id.answerStatus);

   /////////////////////////////////////////////////////////////////////


        timer();


        toolUname=findViewById(R.id.toolbar_uName);



//////////////////////////////////check correct answer////////////
/// ////////////////////////(correct answer need to be /////////////
/// ////////////////////////set currently static on 1)//////////
        answer1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                countDownTimer.cancel();
                if (ans1.get(questioncount)==correctans.get(questioncount)){
                    answerAlertBox(1);
                }
                else {
                    answerAlertBox(0);
                }
            }
        });
        answer2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                countDownTimer.cancel();
                if (ans2.get(questioncount)==correctans.get(questioncount)){
                    answerAlertBox(1);
                }
                else {
                    answerAlertBox(0);
                }
            }
        });
        answer3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                countDownTimer.cancel();
                if (ans3.get(questioncount)==correctans.get(questioncount)){
                    answerAlertBox(1);
                }
                else {
                    answerAlertBox(0);
                }
            }
        });
        answer4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                countDownTimer.cancel();
                if (ans4.get(questioncount)==correctans.get(questioncount)){
                    answerAlertBox(1);
                }
                else {
                    answerAlertBox(0);
                }
            }
        });
    }

//////////////////////show correct,incorrect and misser answer popup /////////////


    private void answerAlertBox(int flag)
    {
        questioncount++;

        if (questioncount<=15){
            next.setText("Next Question");

            if (flag==1){
                correctcount++;
                status.setText("Correct Answer");
                status.setTextColor(getResources().getColor(R.color.material_green));
                next.setBackgroundColor(getResources().getColor(R.color.material_green));
            }
            else if (flag==0){
                status.setText("Oops! Inorrect Answer");
                status.setTextColor(getResources().getColor(R.color.red500));
                next.setBackgroundColor(getResources().getColor(R.color.red500));
            }
            else {
                missedcount++;
                status.setText("Oops! Time Out");
                status.setTextColor(getResources().getColor(R.color.red500));
                next.setBackgroundColor(getResources().getColor(R.color.red500));
            }
            next.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    setQuestion();
                    questionNumber.setText(String .valueOf(questioncount)+"/15");
                    dialog.dismiss();
                    timer();
                }
            });

        }


        else {
            if (flag==1){
                correctcount++;
                status.setText("Correct answer\nQuiz Completed");
                status.setTextColor(getResources().getColor(R.color.material_green));
                next.setBackgroundColor(getResources().getColor(R.color.material_green));
                next.setText("See Result");
                next.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                        Intent intent=new Intent(Quiz.this,QuizResult.class);
                        intent.putExtra("correct", correctcount);
                        intent.putExtra("missed", missedcount);


                        startActivity(intent);
                        finish();
                    }
                });
            }
            else {
                status.setText("InCorrect answer\nQuiz Completed");
                status.setTextColor(getResources().getColor(R.color.red500));
                next.setBackgroundColor(getResources().getColor(R.color.material_green));
                next.setText("See Result");
                next.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                        Intent intent=new Intent(Quiz.this,QuizResult.class);
                        intent.putExtra("correct", correctcount);
                        intent.putExtra("missed", missedcount);


                        startActivity(intent);
                        finish();
                    }
                });
            }


        }


       

        dialog.show();
    }


    @Override
    protected void onResume() {
        countDownTimer.start();
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        ////////////////cancel timer on activity pause//////////////////
        countDownTimer.cancel();
    }



    ///////////////////timer for quiz////////////////////////////
    public void timer(){
        countDownTimer=new CountDownTimer(30000, 1000) {
            @Override
            public void onTick(long l) {

                if (l/1000>=10){
                    toolUname.setText("00:"+String.valueOf(l/1000));
                }
                else {
                    toolUname.setText("00:0"+String.valueOf(l/1000));

                }
            }

            @Override
            public void onFinish() {
                answerAlertBox(2);

            }
        }.start();
    }



//    set questions and answers//////////////////////
    private void getQuestions(){
        quizlist= (ArrayList<QuizModel>) getIntent().getSerializableExtra("quizlist");

        for (int i=0;i<quizlist.size();i++){
            quizModel=quizlist.get(i);
            ArrayList<String> ans=new ArrayList<>();

            ans.add(quizModel.getAnswer1());
            ans.add(quizModel.getAnswer2());
            ans.add(quizModel.getAnswer3());
            ans.add(quizModel.getAnswer4());

            Collections.shuffle(ans);


            questions.add(quizModel.getQuestion());
            ans1.add(ans.get(0));
            ans2.add(ans.get(1));
            ans3.add(ans.get(2));
            ans4.add(ans.get(3));
            correctans.add(quizModel.getCorrect_answer());
        }
        setQuestion();
    }


    private void setQuestion(){
        question.setText(Html.fromHtml(questions.get(questioncount)));
        answer1.setText(Html.fromHtml(ans1.get(questioncount)));
        answer2.setText(Html.fromHtml(ans2.get(questioncount)));
        answer3.setText(Html.fromHtml(ans3.get(questioncount)));
        answer4.setText(Html.fromHtml(ans4.get(questioncount)));

    }

    @Override
    public void onBackPressed() {
        //Checking for fragment count on backstack
        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            getSupportFragmentManager().popBackStack();
        } else if (!doubleBackToExitPressedOnce) {
            this.doubleBackToExitPressedOnce = true;



            final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
            alertDialogBuilder.setPositiveButton("Yes",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {Quiz.super.onBackPressed();

                        }
                    });
            alertDialogBuilder.setNegativeButton("No",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {doubleBackToExitPressedOnce = false;
                        }
                    });


            alertDialogBuilder.setMessage("You will loose your current session\nDo you want to exit?");
            alertDialogBuilder.setCancelable(false);
            alertDialogBuilder.show();

        } else {
            super.onBackPressed();
            return;
        }
    }

}
