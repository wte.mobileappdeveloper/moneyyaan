package com.moneyaan.activities;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.moneyaan.R;
import com.moneyaan.configs.Configs;
import com.moneyaan.models.SpamListModel;
import com.moneyaan.recyclerAdapter.SpamAdapter;
import com.moneyaan.utils.InternetConnectivity.BaseActivity;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.moneyaan.configs.Configs.KEY_CLIENT_ID;

public class SpamActivity extends BaseActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener {
    private Toolbar toolbar;
    public ImageView img_cal,no_spam_list_img;
    private int cYear, cMonth, cDay;
    private Spinner sp_mark;
    private String status;
    private TextView txt_current_date, txt_no_task_available,txt_list_heading;
    private Button update_list;
    String reason="";

    public static int checkedStatus=0;

    private RecyclerView spamRecyleView;

    public static final String KEY_USER_ID = "customer_id";
    public static final String KEY_USERDATE = "date";
    public static final String KEY_STATUS = "status";
    public static final String KEY_PACKAGE_ID = "campaign_id";
    public static final String KEY_REASON = "reason";
    public String current_date;
    public String _userId;
    public ProgressDialog progressDialog;
    public String pacageID;
    ArrayAdapter<String> dataAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spam);
        toolbar= findViewById(R.id.toggle_toolbar);
        toolbar.setTitle("Campaign List");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        progressDialog = new ProgressDialog(this);
        update_list=findViewById(R.id.update_list);
        no_spam_list_img=findViewById(R.id.no_spam_list_img);


        //............. list =new ArrayList<>();

        spamRecyleView = (RecyclerView) findViewById(R.id.recycl_view_for_spamlist);
        img_cal = (ImageView) findViewById(R.id.img_cal);

        sp_mark = (Spinner) findViewById(R.id.sp_mark);

        //..........  txt_status = (TextView) findViewById(R.id.txt_status);

        txt_no_task_available = (TextView) findViewById(R.id.txt_no_text_available);
        txt_list_heading=findViewById(R.id.txt_list_heading);
        txt_current_date = (TextView) findViewById(R.id.txt_current_date_spam);

        sp_mark.setOnItemSelectedListener(this);


        List<String> status = new ArrayList<String>();
//        status.add("Select List");
        status.add("Campaign");
        status.add("Spam");

        dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, status);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_mark.setAdapter(dataAdapter);

        SharedPreferences preferences = this.getApplicationContext().getSharedPreferences("login", Context.MODE_PRIVATE);
        _userId = preferences.getString("User_Id", "");
        Log.e("_userId",_userId);

        // Current Date

        String date = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
        txt_current_date.setText(date);
        current_date = date;

        fetchSpamList("Campaign");
    }

    @Override
    public void onClick(View v) {
        if (v == img_cal) {
            // Get Current Date
            final Calendar c = Calendar.getInstance();
            cYear = c.get(Calendar.YEAR);
            cMonth = c.get(Calendar.MONTH);
            cDay = c.get(Calendar.DAY_OF_MONTH);
            DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                    new DatePickerDialog.OnDateSetListener() {

                        @Override
                        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                            current_date = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
                            txt_current_date.setText(current_date);
                            fetchSpamList("Campaign");
                            toolbar.setTitle("Campaign List");
                            // Reset Spinner Value
                            sp_mark.setSelection(0);

                        }
                    }, cYear, cMonth, cDay);

            datePickerDialog.show();

        } else if (v == update_list) {
            if (checkedStatus>0){
                checkedStatus=0;

                if (update_list.getText().toString().equalsIgnoreCase("Spam"))
                {
//                String markSpam = "Add";
                    spamReason();
//                fetchUpdateSpamList(markSpam);
                }else
                {
                    String markSpam = "Remove";
                    updateSpamList(markSpam,"");
                }
            }else {
                Toast.makeText(SpamActivity.this,"Please select atleast one campaign",Toast.LENGTH_SHORT).show();
            }





        }
    }

    private void fetchSpamList(final String statuspara) {
        progressDialog.setMessage("List Showing");
        progressDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Configs.url_SpamList,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("spam Response",response);

                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            if (jsonObject.optString("error").equalsIgnoreCase("true")) {
                                txt_no_task_available.setVisibility(View.VISIBLE);
                                no_spam_list_img.setVisibility(View.VISIBLE);
                                spamRecyleView.setVisibility(View.INVISIBLE);
                                txt_no_task_available.setText(jsonObject.optString("error_msg"));
                                progressDialog.dismiss();
                            }

                            if (jsonObject.optString("error").equalsIgnoreCase("false")) {

                                ArrayList<SpamListModel> list = new ArrayList<>();
                                if (jsonObject.optString("error_msg").equalsIgnoreCase("Success!")) {
                                    try {
                                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                                        for (int i = 0; i < jsonArray.length(); i++) {

                                            SpamListModel spamListModel = new SpamListModel();

                                            JSONObject jsonObj = jsonArray.getJSONObject(i);
                                            String package_ID = jsonObj.getString("campaing_id");
                                            String package_Name = jsonObj.getString("campaing_name");
                                            String icons = jsonObj.getString("icone");

//                              ToDo change static value
//                                           String checktype = jsonObj.getString("type");
                                            String checktype = "false";

// For Set data
                                            spamListModel.setPackage_Name(package_Name);
                                            spamListModel.setPackage_ID(package_ID);
                                            spamListModel.setIcon(icons);
                                            spamListModel.setType(checktype);
                                            list.add(spamListModel);

                                        }
                                        SpamAdapter adapter = new SpamAdapter(SpamActivity.this, list);
                                        adapter.setData(list);
                                        spamRecyleView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                                        spamRecyleView.setAdapter(adapter);
                                        spamRecyleView.setHasFixedSize(true);
                                        progressDialog.dismiss();
                                        txt_no_task_available.setVisibility(View.INVISIBLE);
                                        no_spam_list_img.setVisibility(View.INVISIBLE);
                                        spamRecyleView.setVisibility(View.VISIBLE);
                                    }catch (JSONException e) {
                                        e.fillInStackTrace();
                                    }
                                } else if (jsonObject.optString("error_msg").equalsIgnoreCase("No List found!!")) {

                                    spamRecyleView.setVisibility(View.INVISIBLE);
                                    txt_no_task_available.setVisibility(View.VISIBLE);
                                    no_spam_list_img.setVisibility(View.VISIBLE);
                                    txt_no_task_available.setText(jsonObject.optString("error_msg"));

                                }


                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.e("Erro",e.toString());
                        }
                        progressDialog.dismiss();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(SpamActivity.this, "Empty Task !!", Toast.LENGTH_SHORT).show();
                Log.e("Erro",error.toString());
                progressDialog.dismiss();

            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put(KEY_USER_ID, _userId);
                params.put(KEY_USERDATE, current_date);
                params.put(KEY_STATUS, statuspara);
                params.put(KEY_CLIENT_ID,Configs.clientId(SpamActivity.this));

                Log.e("spam params",params.toString());
                return params;
            }

        };

        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringRequest);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

    }

    private void updateSpamList(final String status, final String spam_reason) {
        RequestQueue requestQueueforUpdateSpam = Volley.newRequestQueue(getApplicationContext());
        StringRequest request = new StringRequest(Request.Method.POST, Configs.url_UpdateSpamList,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("update spam response",response);
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.optString("error").equalsIgnoreCase("false") || jsonObject.optString("error_msg").equalsIgnoreCase("Updated successfully!!!!")) {
                                Toast.makeText(SpamActivity.this, "Updated successfully!!!!", Toast.LENGTH_SHORT).show();

                                fetchSpamList(sp_mark.getSelectedItem().toString());


                            } else if (jsonObject.optString("error").equalsIgnoreCase("false")) {
                                Toast.makeText(SpamActivity.this, "Updated Unsuccessfully!!!!", Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            Log.e("update spam err",e.toString());
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put(KEY_USER_ID, _userId);
                params.put(KEY_PACKAGE_ID, pacageID.substring(0, pacageID.length() - 1));
//                params.put(KEY_PACKAGE_ID, pacageID);
                params.put(KEY_STATUS, status);

//                ToDo add reason in api
                params.put(KEY_REASON, spam_reason);
                params.put(KEY_CLIENT_ID,Configs.clientId(SpamActivity.this));

                Log.e("params spam",params.toString());
                return params;
            }
        };
        requestQueueforUpdateSpam.add(request);
        request.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));



    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long l) {
        status = parent.getItemAtPosition(position).toString();
        if (status.equalsIgnoreCase("Select List"))
        {
            ((TextView) sp_mark.getSelectedView()).setTextColor(Color.WHITE);


        } else if (status.equalsIgnoreCase("Campaign")) {

            ((TextView) sp_mark.getSelectedView()).setTextColor(Color.WHITE);

            update_list.setText("Spam");

            fetchSpamList(status);
            toolbar.setTitle("Campaign List");

            //   img_mark_spam.setVisibility(View.GONE);
            //   img_unmark_spam.setVisibility(View.VISIBLE);

        } else if (status.equalsIgnoreCase("Spam")) {

            ((TextView) sp_mark.getSelectedView()).setTextColor(Color.WHITE);
            update_list.setText("Unspam");
            toolbar.setTitle("Spam List");

            fetchSpamList(status);
            //  img_unmark_spam.setVisibility(View.GONE);
            //  img_mark_spam.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {
    }

    public void addPackageId(ArrayList<SpamListModel> spamListModels) {
        pacageID = "";
        for (SpamListModel spamListModel : spamListModels) {

//            if (pacageID.contains(spamListModel.getPackage_ID())){
//                pacageID.replace(spamListModel.getPackage_ID() + ",","");
//                Log.e("spam pakage remove",pacageID);


//            }else {
            pacageID += spamListModel.getPackage_ID() + ",";
            Log.e("spam pakage add",pacageID);
//            }


        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition( R.anim.ritht_to_left_in, R.anim.ritht_to_left );

    }

    private void spamReason(){

        final Dialog dialog = new Dialog(SpamActivity.this,R.style.floatingDialog);
        dialog.setContentView(R.layout.dialog_spam_reason);
        dialog.setCancelable(true);
        final RadioButton reason1,reason2,otherreason;
        Button button;
        final RadioGroup radioGroup;
        final EditText other;
        button=dialog.findViewById(R.id.dialogbutton);
        reason1=dialog.findViewById(R.id.radioreason1);
        reason2=dialog.findViewById(R.id.radio_reason2);
        radioGroup=dialog.findViewById(R.id.RGroup);
        otherreason=dialog.findViewById(R.id.radio_other_reason);
        other=dialog.findViewById(R.id.txt_other);

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch(checkedId){
                    case R.id.radioreason1:
                        other.setVisibility(View.GONE);
                        reason=reason1.getText().toString();

                        break;
                    case R.id.radio_reason2:
                        reason=reason2.getText().toString();
                        other.setVisibility(View.GONE);
                        break;
                    case R.id.radio_other_reason:

                        other.setVisibility(View.VISIBLE);


                        break;
                }
            }
        });


        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (radioGroup.getCheckedRadioButtonId() == -1) {
                    Toast.makeText(SpamActivity.this,"Please select a reason",Toast.LENGTH_SHORT).show();

                } else {


                    if (other.getVisibility() == View.VISIBLE) {
                        reason = other.getText().toString();
                    }

                    String markSpam = "Y";
                    updateSpamList(markSpam, reason);
                    dialog.dismiss();
                }
            }
        });



        dialog.show();



    }


}
