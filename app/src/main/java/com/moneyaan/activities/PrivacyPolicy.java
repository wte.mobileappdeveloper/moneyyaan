package com.moneyaan.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.moneyaan.R;
import com.moneyaan.utils.InternetConnectivity.BaseActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.moneyaan.configs.Configs.KEY_CLIENT_ID;
import static com.moneyaan.configs.Configs.url_acceptPolicy;


public class PrivacyPolicy extends BaseActivity {
    private ProgressDialog progressDialog;
    public static final String VISIBILITY_OF_POLICY_BUTTON="visibility";
    public static final String KEY_CUSTMRID = "customer_id";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_privacy_policy);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading...");


        Button button= findViewById(R.id.accept_btn);

        WebView view=findViewById(R.id.webView);
        view.loadUrl("http://www.moneyyaan.com/privacy-and-data-policy.html");
        view.getSettings().setJavaScriptEnabled(true);
        view.setWebViewClient(new WebViewClient() {
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
//                Toast.makeText(activity, description, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon)
            {
                progressDialog.show();
            }


            @Override
            public void onPageFinished(WebView view, String url) {
                progressDialog.dismiss();


            }

        });
        try {
            button.setVisibility(getIntent().getIntExtra(VISIBILITY_OF_POLICY_BUTTON, View.VISIBLE));
        }catch (Exception ignored){}

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                updatePolicy();
            }
        });
    }



    void updatePolicy(){
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url_acceptPolicy,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {


                        try {
                            JSONObject jsonObject=new JSONObject(response);
                            if (jsonObject.getBoolean("error")){
                                Toast.makeText(PrivacyPolicy.this, "Some Error Occured", Toast.LENGTH_SHORT).show();
                            }else {
                                Intent intent=new Intent(PrivacyPolicy.this,Home.class);
                                startActivity(intent);
                                finish();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            progressDialog.dismiss();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                SharedPreferences preferences = getSharedPreferences("login", Context.MODE_PRIVATE);
                String _userId = preferences.getString("User_Id", "");
                Map<String, String> params = new HashMap<>();
                params.put(KEY_CUSTMRID, _userId);
                params.put(KEY_CLIENT_ID, "1");
                return params;
            }

        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    }
}
