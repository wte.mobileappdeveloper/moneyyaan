package com.moneyaan.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.moneyaan.R;
import com.moneyaan.models.QuizModel;
import com.moneyaan.utils.InternetConnectivity.BaseActivity;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class QuizRules extends BaseActivity implements View.OnClickListener {
    ArrayList<QuizModel> quizlist=new ArrayList<>();
    int category;
    Spinner ct_spinner;
    ImageView rules;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz_rules);


        ////////////////toolbaar////////////////////

        Toolbar toolbar= (Toolbar) findViewById(R.id.toggle_toolbar);
        TextView toolactName= findViewById(R.id.act_name);
        ct_spinner=findViewById(R.id.cat_spinner);
        toolactName.setText("Quiz Categories");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        rules=findViewById(R.id.toolbar_right_image);
        rules.setImageResource(R.drawable.ic_action_info_non_vector);

        rules.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                howItWorksPopup(QuizRules.this);
            }
        });


        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        initControl();
//        loadSpinner();
    }


    private void loadQuiz(String category) {
        final ProgressDialog progressDialog=new ProgressDialog(this);
        progressDialog.setMessage("Loading Quiz");
        progressDialog.setCancelable(false);
        progressDialog.show();

        String url="https://opentdb.com/api.php?" +
                "amount=16" +
                "&category=" +category+
                "&difficulty=easy" +
                "&type=multiple";

        final RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());

        StringRequest stringRequest = new StringRequest(com.android.volley.Request.Method.GET, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.e("rtesponse",response);

                try {
                    quizlist.clear();
                    JSONObject jsonObject=new JSONObject(response);
                    if (jsonObject.getString("response_code").equalsIgnoreCase("0"))
                    {
                        JSONArray results=jsonObject.getJSONArray("results");
                        for (int i=0;i<results.length();i++){

                            QuizModel quizModel=new QuizModel();

                            JSONObject jsn=results.getJSONObject(i);

                            quizModel.setQuestion(jsn.getString("question"));
                            quizModel.setCorrect_answer(jsn.getString("correct_answer"));
                            quizModel.setAnswer1(jsn.getString("correct_answer"));
                            quizModel.setAnswer2((jsn.getJSONArray("incorrect_answers").getString(0)));
                            quizModel.setAnswer3((jsn.getJSONArray("incorrect_answers").getString(1)));
                            quizModel.setAnswer4((jsn.getJSONArray("incorrect_answers").getString(2)));


                            quizlist.add(quizModel);

                        }
                        Intent intent=new Intent(QuizRules.this,Quiz.class);
                        intent.putExtra("quizlist",quizlist);
                        startActivity(intent);
                    }
                    else if (jsonObject.getString("response_code").equalsIgnoreCase("1")){

                        ct_spinner.setSelection(0);
                        Toast.makeText(QuizRules.this,"No Questions Right Now",Toast.LENGTH_SHORT).show();
                    }

                        progressDialog.dismiss();


                } catch (Exception e) {
                    Log.e("error",e.toString());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();

            }
        });
        requestQueue.add(stringRequest);
    }

    @Override
    public void onClick(View v) {


        switch (v.getId())
        {


            case R.id.general :
                category=9;
                break;

            case R.id.books:
                category=10;
                break;

            case R.id.film:
                category=11;
                break;

            case R.id.theatre:
                category=13;
                break;

            case R.id.television:
                category=14;
                break;

            case R.id.videoGames:
                category=15;
                break;

            case R.id.boardGames:
                category=16;
                break;

            case R.id.scie_nature:
                category=17;
                break;

            case R.id.computer:
                category=18;
                break;

            case R.id.maths:
                category=19;
                break;

            case R.id.mythalogy:
                category=20;
                break;

            case R.id.sports:
                category=21;
                break;

            case R.id.geography:
                category=22;
                break;

            case R.id.history:
                category=23;
                break;

            case R.id.politics:
                category=24;
                break;

            case R.id.arts:
                category=25;
                break;

            case R.id.celebs:
                category=26;
                break;

            case R.id.animals:
                category=27;
                break;

            case R.id.vehicles:
                category=28;
                break;

            case R.id.comics:
                category=29;
                break;

            case R.id.gadget:
                category=30;
                break;


        }



        loadQuiz(String.valueOf(category));
    }


    private void initControl(){
        findViewById(R.id.general).setOnClickListener(this);
        findViewById(R.id.books).setOnClickListener(this);
        findViewById(R.id.film).setOnClickListener(this);
        findViewById(R.id.theatre).setOnClickListener(this);
        findViewById(R.id.television).setOnClickListener(this);
        findViewById(R.id.videoGames).setOnClickListener(this);
        findViewById(R.id.boardGames).setOnClickListener(this);
        findViewById(R.id.scie_nature).setOnClickListener(this);
        findViewById(R.id.computer).setOnClickListener(this);
        findViewById(R.id.maths).setOnClickListener(this);
        findViewById(R.id.mythalogy).setOnClickListener(this);
        findViewById(R.id.sports).setOnClickListener(this);
        findViewById(R.id.geography).setOnClickListener(this);
        findViewById(R.id.history).setOnClickListener(this);
        findViewById(R.id.politics).setOnClickListener(this);
        findViewById(R.id.arts).setOnClickListener(this);
        findViewById(R.id.celebs).setOnClickListener(this);
        findViewById(R.id.animals).setOnClickListener(this);
        findViewById(R.id.vehicles).setOnClickListener(this);
        findViewById(R.id.comics).setOnClickListener(this);
        findViewById(R.id.gadget).setOnClickListener(this);

    }


//    private void loadSpinner(){
//        List<String> spinner_item = new ArrayList<String>();
//        spinner_item.add("Select a Category");
//        spinner_item.add("Animals");
//        spinner_item.add("Arts");
//        spinner_item.add("Board Games");
//        spinner_item.add("Books");
//        spinner_item.add("Celebrities");
//        spinner_item.add("Comics");
//        spinner_item.add("Computer");
//        spinner_item.add("Films");
//        spinner_item.add("Gadgets");
//        spinner_item.add("Geography");
//        spinner_item.add("General");
//        spinner_item.add("History");
//        spinner_item.add("Mathematics");
//        spinner_item.add("Mythology");
//        spinner_item.add("Politics");
//        spinner_item.add("Science & Nature");
//        spinner_item.add("Sports");
//        spinner_item.add("Television");
//        spinner_item.add("Theatre");
//        spinner_item.add("Vehicles");
//        spinner_item.add("Video Games");
//
//
//
//        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, spinner_item);
//        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        ct_spinner.setAdapter(dataAdapter);
//
//        ct_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
//            @Override
//            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
//
//                ((TextView) ct_spinner.getSelectedView()).setTextColor(getResources().getColor(R.color.white));
//                String name=ct_spinner.getSelectedItem().toString();
//                switch (name)
//                {
//
//
//                    case "General" :
//                        category=9;
//                        break;
//
//                    case "Books":
//                        category=10;
//                        break;
//
//                    case "Films":
//                        category=11;
//                        break;
//
//                    case "Theatre":
//                        category=13;
//                        break;
//
//                    case "Television":
//                        category=14;
//                        break;
//
//                    case "Video Games":
//                        category=15;
//                        break;
//
//                    case "Board Games":
//                        category=16;
//                        break;
//
//                    case "Science & Nature":
//                        category=17;
//                        break;
//
//                    case "Computer":
//                        category=18;
//                        break;
//
//                    case "Mathematics":
//                        category=19;
//                        break;
//
//                    case "Mythology":
//                        category=20;
//                        break;
//
//                    case "Sports":
//                        category=21;
//                        break;
//
//                    case "Geography":
//                        category=22;
//                        break;
//
//                    case "History":
//                        category=23;
//                        break;
//
//                    case "Politics":
//                        category=24;
//                        break;
//
//                    case "Arts":
//                        category=25;
//                        break;
//
//                    case "Celebrities":
//                        category=26;
//                        break;
//
//                    case "Animals":
//                        category=27;
//                        break;
//
//                    case "Vehicles":
//                        category=28;
//                        break;
//
//                    case "Comics":
//                        category=29;
//                        break;
//
//                    case "Gadgets":
//                        category=30;
//                        break;
//
//                    case "Select a Category":
//                        return;
//
//
//                }
//
//
//
//                loadQuiz(String.valueOf(category));
//
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> adapterView) {
//
//            }
//        });
//
//
//    }



    private void howItWorksPopup(final Activity activity){
        final AlertDialog share;

        final AlertDialog.Builder adb = new AlertDialog.Builder(activity, R.style.floatingDialog);
        View view = activity.getLayoutInflater().inflate(R.layout.how_it_work_popup, null);
        adb.setView(view);
        share = adb.create();
        share.getWindow().getAttributes().windowAnimations = R.style.AppTheme;

        ImageView cancel;
        cancel=view.findViewById(R.id.hw_it_works_cancel);
        LinearLayout hw_it_wrks;
        hw_it_wrks=view.findViewById(R.id.how_it_work_popup);
        hw_it_wrks.setVisibility(View.GONE);
        view.findViewById(R.id.quizRules_popup).setVisibility(View.VISIBLE);

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    share.dismiss();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        share.show();
    }

}
