package com.moneyaan.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.moneyaan.R;
import com.moneyaan.configs.Configs;
import com.moneyaan.utils.InternetConnectivity.BaseActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.moneyaan.activities.EarnOffers.KEY_UID;
import static com.moneyaan.configs.Configs.KEY_CLIENT_ID;


public class Support extends BaseActivity {
    private Toolbar toolbar;
    public EditText txt_name, txt_mobile, txt_subject, txt_query;
    Button btn_ok, btn_cancel;
    public static final String KEY_CNAME = "Name";
    public static final String KEY_MOBILE = "Mobileno";
    public static final String KEY_SUBJECT = "subject";
    public static final String KEY_QUERY = "complaintext";
    ProgressDialog progressDoalog;
    private SharedPreferences sharedPreferences;
    String sp_name, sp_mobile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_support);
        toolbar = (Toolbar) findViewById(R.id.toggle_toolbar);
        toolbar.setTitle("Support");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        sharedPreferences = getSharedPreferences("login", Context.MODE_PRIVATE);
        sp_name = sharedPreferences.getString("Name", "");
        sp_mobile = sharedPreferences.getString("Mobileno", "");

        txt_name = (EditText) findViewById(R.id.txt_customer_name);
        txt_mobile = (EditText) findViewById(R.id.txt_cust_mobile);
        txt_subject = (EditText) findViewById(R.id.txt_cust_subject);
        txt_query = (EditText) findViewById(R.id.txt_cus_query);
        btn_ok = (Button) findViewById(R.id.btn_ok);
        btn_cancel = (Button) findViewById(R.id.btn_cancel);

        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                txt_subject.setText("");
                txt_query.setText("");
            }
        });
        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Validate();

            }
        });

        txt_name.setText(sp_name);
        txt_mobile.setText(sp_mobile);
        txt_name.setEnabled(false);
        txt_mobile.setEnabled(false);
    }

    private void Validate() {

        if (txt_name.getText().toString() == null || txt_name.getText().length() == 0) {
            Toast.makeText(this, "please Enter Name", Toast.LENGTH_SHORT).show();
        } else if (txt_mobile.getText().toString() == null || txt_mobile.getText().length() == 0) {
            Toast.makeText(this, "Please Enter Mobile Number", Toast.LENGTH_SHORT).show();
        } else if (txt_subject.getText().toString() == null || txt_subject.getText().length() == 0) {
            Toast.makeText(this, "Please Enter Subject", Toast.LENGTH_SHORT).show();
        } else if (txt_query.getText().toString() == null || txt_query.getText().length() == 0) {
            Toast.makeText(this, "Please Enter Query", Toast.LENGTH_SHORT).show();
        } else {

            complainUser();
        }

    }
    ///////////////////////////parse post request////////////////////////
    private void complainUser() {
        final String cname = txt_name.getText().toString();
        final String cmobile = txt_mobile.getText().toString();
        final String subject = txt_subject.getText().toString();
        final String query = txt_query.getText().toString();
        progressDoalog = new ProgressDialog(Support.this);
        progressDoalog.setMessage("Please wait while submitting your request");
        progressDoalog.show();



        StringRequest stringRequest = new StringRequest(Request.Method.POST, Configs.url_Support,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.e("support_response",response.toString());
                        try {
                            JSONObject jObject = new JSONObject(response);
                            if (jObject.optString("error").toString().equalsIgnoreCase("false")) {
                                Toast.makeText(Support.this, "Your request has been registered successfully !!", Toast.LENGTH_SHORT).show();

                                Intent loginIntent = new Intent(Support.this, Home.class);
                                startActivity(loginIntent);
                                finish();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        progressDoalog.dismiss();
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(Support.this,"Some error occurred", Toast.LENGTH_SHORT).show();
                        progressDoalog.dismiss();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
//                params.put(KEY_CNAME, cname);

                SharedPreferences preferences = getSharedPreferences("login", Context.MODE_PRIVATE);
                params.put("customer_id", preferences.getString("User_Id", ""));

//                Todo add status in api
                params.put(KEY_SUBJECT, subject);
                params.put(KEY_UID, getSharedPreferences("login", Context.MODE_PRIVATE).getString("User_Id", ""));
                params.put(KEY_QUERY, query);
                params.put(KEY_CLIENT_ID,Configs.clientId(Support.this));
                Log.e("support params",params.toString());
                return params;
            }

        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition( R.anim.ritht_to_left_in, R.anim.ritht_to_left );

    }
}
