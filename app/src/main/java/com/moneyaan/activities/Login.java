package com.moneyaan.activities;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.moneyaan.configs.Configs;
import com.moneyaan.utils.InternetConnectivity.BaseActivity;
import com.moneyaan.utils.InternetStatus;
import com.moneyaan.R;
import com.moneyaan.CommonFunction;
import com.onesignal.OneSignal;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.moneyaan.configs.Configs.KEY_CLIENT_ID;
import static com.moneyaan.configs.Configs.clientId;


public class Login extends BaseActivity {

    ImageView img_show;
    boolean pwdststus=true;
    EditText edt_password,edt_mobile;
    Button login,signup;
    TextView frgtPswrd;
    boolean rememberMe;
    private SharedPreferences myPrefsForSesssion;
    private ProgressDialog progressBar;
    public static final String KEY_MOBILE = "mobile";
    public static final String KEY_PASSWORD = "password";
    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 1;
    String advertisingId="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);

        setContentView(R.layout.activity_login);
        img_show=findViewById(R.id.img_show);
        edt_password=findViewById(R.id.pasword);
        edt_mobile=findViewById(R.id.usrname);
        login=findViewById(R.id.login);
        signup=findViewById(R.id.signup);
        frgtPswrd=findViewById(R.id.frgtPassword);
        progressBar=new ProgressDialog(this);


        getGAID();


        myPrefsForSesssion = getSharedPreferences("session", Context.MODE_PRIVATE);
        rememberMe = myPrefsForSesssion.getBoolean("remember", false);

        if (rememberMe) {
            Intent i = new Intent(Login.this, Home.class);
            startActivity(i);
            finish();

        }else {

            OneSignal.setSubscription(false);

        }

        img_show.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (pwdststus)
                {
                    edt_password.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                    pwdststus=false;
//                    Drawable img =getApplication().getResources().getDrawable( R.drawable.eye_close_up);
//                    img_show.setImageDrawable(img);
                    img_show.setImageResource(R.drawable.eye_close_up);

                }else {
                    edt_password.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    pwdststus=true;
//                    Drawable img =getApplication().getResources().getDrawable( R.drawable.eye);
//                    img_show.setImageDrawable(img);
                    img_show.setImageResource(R.drawable.eye);

                }
            }
        });


        frgtPswrd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Login.this,ForgetPassword.class));
            }
        });
        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Login.this,SignUp.class));
            }
        });

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                loginValidation();
                Log.e("adid",advertisingId);

            }
        });
        checkConnection();


    }

    //////////////login validattion///////////
    private void loginValidation() {
        if (edt_mobile.getText().toString() == null || edt_mobile.getText().length() == 0 || !CommonFunction.isValidMobile(edt_mobile.getText().toString())) {
            Toast.makeText(this, "Enter Valid Mobile Number", Toast.LENGTH_SHORT).show();
            edt_mobile.requestFocus();
        }else if (edt_password.getText().toString() == null || edt_password.getText().toString().length() == 0) {
            Toast.makeText(this, "Enter Password", Toast.LENGTH_SHORT).show();
            edt_password.requestFocus();
        }

        else if (edt_mobile.getText().toString().charAt(0)=='0'){
            Toast.makeText(this, "Please remove 0 from number", Toast.LENGTH_SHORT).show();

        }
        else
        {
            if (InternetStatus.getInstance(this).isOnline()) {
                loginParse();

            } else {
                String message;
                Snackbar snackbar;
                int color;
                color = Color.RED;
                message = "Sorry Check Your Internet Connection !!";

                snackbar = Snackbar.make(findViewById(R.id.login_relative_layout), message, Snackbar.LENGTH_SHORT);
                View sbView = snackbar.getView();
//                TextView textView =  sbView.findViewById(android.support.design.R.id.snackbar_text);
//                textView.setTextColor(color);
                snackbar.show();

            }
        }
    }

    //    ///////////////////check wether connected to internet or not////////////////
    private void checkConnection() {
        String message;
        Snackbar snackbar;
        int color;
        if (InternetStatus.getInstance(this).isOnline()) {
            message = "Good! Connected to Internet";
            color = Color.GREEN;
            //fetchTodayTask();
        } else {
            message = "Sorry Check Your Internet Connection !!";
            // txt_no_task_available.setText(message);
            color = Color.RED;
            //  mSwipeRefreshLayout.setRefreshing(false);
        }
        snackbar = Snackbar.make(findViewById(R.id.login_relative_layout), message, Snackbar.LENGTH_SHORT);
        View sbView = snackbar.getView();
//        TextView textView =  sbView.findViewById(android.support.design.R.id.snackbar_text);
//        textView.setTextColor(color);
        snackbar.show();
    }
/////////////////////////////////parse login details///////////////////////

    private void loginParse() {

        final String mobile = edt_mobile.getText().toString();
        final String password = edt_password.getText().toString();

        // Device Address

        progressBar = new ProgressDialog(this);
        progressBar.setCancelable(true);
        progressBar.setMessage("Logging in.....");
        progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressBar.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Configs.url_login,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            Log.e("login Respionse",response);
                            JSONObject jObject = new JSONObject(response);
                            if (jObject.optString("error").equalsIgnoreCase("false") || jObject.optString("Message").equalsIgnoreCase("Login successfully")) {
                                String _User_Id = jObject.getString("customer_id");
                                String _Name = jObject.getString("name");
                                String _Mobileno = jObject.getString("mobile");
                                String _EmailId = jObject.getString("email");
                                String _Gender = jObject.getString("gender");
                                String _dob = jObject.getString("dob");
                                String _state = jObject.getString("state");
                                String city = jObject.getString("city");
                                String verify_step1 = jObject.getString("verify_step1");
                                String verify_step2 = jObject.getString("verify_step2");
                                String verify_step3 = jObject.getString("verify_step3");

                                if (_User_Id != null) {

                                    saveLoginSharedPref(_User_Id, _Name, _Mobileno, _EmailId, _Gender,_dob,_state, city);
                                    if (verify_step1.equalsIgnoreCase("Y") && verify_step2.equalsIgnoreCase("Y") && verify_step3.equalsIgnoreCase("Y"))
                                    {
                                        myPrefsForSesssion = getSharedPreferences("session", Context.MODE_PRIVATE);
                                        SharedPreferences.Editor editor1 = myPrefsForSesssion.edit();
                                        editor1.putBoolean("remember", true);
                                        editor1.putBoolean("verifyUser",true);
                                        editor1.apply();

                                        startActivity(new Intent(Login.this,Home.class));
                                        finishAffinity();

                                    }else {

                                        startActivity(new Intent(Login.this,VerifiedUserTask.class));
                                        finishAffinity();
                                    }

                                    progressBar.dismiss();
                                    finish();
                                }
                            } else if (jObject.optString("error").equalsIgnoreCase("true") || jObject.optString("error_msg").equalsIgnoreCase("Login credentials are wrong OR Waiting for admin aproval !!")) {
                                progressBar.dismiss();
                                Toast.makeText(Login.this, "Check Your Mobile Number or Password", Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            Log.e("login exception",response);
                            progressBar.dismiss();
                            e.printStackTrace();
                        }
                    }
                },
                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("login Respionse",error.toString());

                        Toast.makeText(Login.this, "Server is not responding !!!", Toast.LENGTH_SHORT).show();
                        progressBar.dismiss();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put(KEY_MOBILE, mobile);
                params.put(KEY_PASSWORD, password);
                params.put("gaid", advertisingId);
                params.put(KEY_CLIENT_ID, clientId(Login.this));
//
                Log.e("Login",params.toString());
                return params;
            }

        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

    }





    private void saveLoginSharedPref(String User_Id, String Name, String Mobileno, String Emailid, String Gender, String dob, String _state, String city) {

        SharedPreferences myPrefsLogin = getSharedPreferences("login", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = myPrefsLogin.edit();
        editor.putString("User_Id", User_Id);
        editor.putString("Name", Name);
        editor.putString("Mobileno", Mobileno);
        editor.putString("EmailId", Emailid);
        editor.putString("Gender",Gender);
        editor.putString("dob", dob);
        editor.putString("_state", _state);
        editor.putString("city", city);
        editor.putString("Sh_ReferEarn", "");
        editor.putString("Sh_Points", "");
        editor.putString("Sh_Notification", "");
        editor.putString("Sh_AdMod", "");
        editor.putBoolean("bonus",true);

        editor.apply();

//          for Session Prefrence



    }







    public void getGAID(){
        Thread thread = new Thread() {
            @Override
            public void run() {
                try {
                    AdvertisingIdClient.Info adInfo = AdvertisingIdClient.getAdvertisingIdInfo(getApplicationContext());
                    advertisingId = adInfo != null ? adInfo.getId() : null;

                    Log.e("gaid in",advertisingId);
                } catch (IOException | GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException exception) {
                    exception.printStackTrace();
                    Log.e("exep gaid",exception.toString());
                }
            }
        };

        // call thread start for background process
        thread.start();

    }






}
