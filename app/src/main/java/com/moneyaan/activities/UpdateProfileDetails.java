package com.moneyaan.activities;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.moneyaan.R;
import com.moneyaan.configs.Configs;
import com.moneyaan.utils.InternetConnectivity.BaseActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.moneyaan.configs.Configs.KEY_CLIENT_ID;
import static com.moneyaan.configs.Configs.clientId;

public class UpdateProfileDetails extends BaseActivity {
    private EditText txt_UDname,confirmAcNo,pancard, txt_UDemail, txt_UDMobile, txt_UDAcNumber, txt_UDBranchName, txt_UDBranchAddress, txt_UDIFSCCode;

    public String userID, email, mobile, account_no, bank_name, branch_address, ifsc_code, user_name;
    public static final String KEY_USER_ID = "userId";
    public static final String KEY_MOBILE = "Mobileno";
    public static final String KEY_NAME = "Name";
    public static final String KEY_EmailId = "EmailId";
    public static final String KEY_ACNUMBER = "Accountno";
    public static final String KEY_BANKNAME = "BankName";
    public static final String KEY_BRANCHADDRSS = "BranchAddress";
    public static final String KEY_IFSC = "IFSCCode";
    public SharedPreferences preferences;
    private ProgressDialog progressDialog;

    ImageView profileimg;

    private SharedPreferences myProfilePref;
    private String user_Name, userId, gender, webmobileuser,mobileNo;
    private Button btn_update,btn_cancel;
    private Dialog dialog;
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_profile_details);


        profileimg=findViewById(R.id.profileImage);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toggle_toolbar);
        TextView toolactName= findViewById(R.id.act_name);
        toolactName.setText("Update Profile");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        dialog=new Dialog(getApplication());
        confirmAcNo=findViewById(R.id.txt_Cnfrm_UDAcNo);
        progressBar=findViewById(R.id.ifsc_chec_progress);
        txt_UDname = (EditText) findViewById(R.id.txt_UDuser_Name);
        txt_UDemail = (EditText) findViewById(R.id.txt_UDemail);
        txt_UDMobile = (EditText) findViewById(R.id.txt_UDphone);
        txt_UDAcNumber = (EditText) findViewById(R.id.txt_UDAcNo);
        txt_UDBranchName = (EditText) findViewById(R.id.txt_UDBranchName);
        txt_UDBranchAddress = (EditText) findViewById(R.id.txt_UDBranchAddress);
        pancard=findViewById(R.id.txt_UDPan);
        txt_UDIFSCCode = (EditText) findViewById(R.id.txt_UPIfcs_code);
        preferences = getSharedPreferences("login", Context.MODE_PRIVATE);
        //This preference for ac details
        SharedPreferences acShared= getSharedPreferences("AC_DETAILS",Context.MODE_PRIVATE);
        userID = preferences.getString("User_Id", "");
        mobile = preferences.getString("Mobileno", "");
        email = preferences.getString("EmailId", "");
        user_name = preferences.getString("Name", "");

        txt_UDname.setText(user_name);
        txt_UDname.setEnabled(false);
        txt_UDMobile.setText(mobile);
        txt_UDMobile.setEnabled(false);
        txt_UDemail.setText(email);
        txt_UDemail.setEnabled(false);

        txt_UDAcNumber.setText(acShared.getString("AC_NUMBER",""));
        txt_UDBranchName.setText(acShared.getString("BANK_NAME",""));
        txt_UDBranchAddress.setText(acShared.getString("BRANCH_ADDRESS",""));
        txt_UDIFSCCode.setText(acShared.getString("IFSC_CODE",""));

        txt_UDIFSCCode.addTextChangedListener(ifscWathcer);

        progressDialog = new ProgressDialog(this);


        //       Toast.makeText(UpdateProfileDetails.this,userID,Toast.LENGTH_SHORT).show();
//        userID=sharedPreferences.getString("User_Id","");

        btn_update = (Button) findViewById(R.id.btn_update);
        btn_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Validate();
            }
        });

        btn_cancel=findViewById(R.id.btn_cancel);
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        getGender();
    }

    private void Validate() {

        if (txt_UDname.getText().toString() == null || txt_UDname.getText().length() == 0) {
            Toast.makeText(this, "Enter User Name", Toast.LENGTH_SHORT).show();
        } else if (txt_UDemail.getText().toString() == null || txt_UDemail.getText().length() == 0) {
            Toast.makeText(this, "Enter Email Id", Toast.LENGTH_SHORT).show();
        } else if (txt_UDMobile.getText().toString() == null || txt_UDMobile.getText().length() == 0) {
            Toast.makeText(this, "Enter Mobile Number", Toast.LENGTH_SHORT).show();
        } else if (txt_UDAcNumber.getText().toString() == null || txt_UDAcNumber.getText().length() == 0) {
            Toast.makeText(this, "Enter Your Account Number", Toast.LENGTH_SHORT).show();
        }  else if (!txt_UDAcNumber.getText().toString().equals(confirmAcNo.getText().toString())) {
            Toast.makeText(this, "Account Number Does Not Match", Toast.LENGTH_SHORT).show();
        } else if (txt_UDBranchName.getText().toString() == null || txt_UDBranchName.getText().length() == 0) {
            Toast.makeText(this, "Enter Your Bank Name", Toast.LENGTH_SHORT).show();
        } else if (txt_UDBranchAddress.getText().toString() == null || txt_UDBranchAddress.getText().length() == 0) {
            Toast.makeText(this, "Enter Your Branch Address", Toast.LENGTH_SHORT).show();
        } else if (txt_UDIFSCCode.getText().toString() == null || txt_UDIFSCCode.getText().length() == 0) {
            Toast.makeText(this, "Enter IFSC Code", Toast.LENGTH_SHORT).show();
        } else {
            confirmDialog(UpdateProfileDetails.this);


        }
    }

    private void updateUserDetails() {
        progressDialog.setMessage("Updating...");
        progressDialog.show();
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(true);

        // Take date From users

        final String uname = txt_UDname.getText().toString();
        final String email = txt_UDemail.getText().toString();
        final String mobile = txt_UDMobile.getText().toString();
        final String accNumber = txt_UDAcNumber.getText().toString();
        final String branchName = txt_UDBranchName.getText().toString();
        final String branchAddress = txt_UDBranchAddress.getText().toString();
        final String ifcsCode = txt_UDIFSCCode.getText().toString();
        final String panNo = pancard.getText().toString();


        StringRequest stringRequest = new StringRequest(Request.Method.POST, Configs.url_UpdateUserBankDetails,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            JSONObject jObject = new JSONObject(response);
                            if (jObject.optString("error").toString().equalsIgnoreCase("false")) {
                                Toast.makeText(UpdateProfileDetails.this, "Profile Updated Successfully!!", Toast.LENGTH_SHORT).show();
                                progressDialog.dismiss();

                                // This function calll for save bank details

                                saveAccountDetails(accNumber,branchName,branchAddress,ifcsCode,panNo);

                                Intent intent = new Intent(UpdateProfileDetails.this, Home.class);
                                startActivity(intent);

                                finish();
                            } else if (jObject.optString("error").toString().equalsIgnoreCase("true")) {
                                Toast.makeText(UpdateProfileDetails.this, "Oops!! some error occured while updating", Toast.LENGTH_SHORT).show();
                                progressDialog.dismiss();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                            progressDialog.dismiss();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(UpdateProfileDetails.this, "Server is  not Responding !!", Toast.LENGTH_SHORT).show();
                        progressDialog.hide();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("customer_id", userID);
                params.put("mobile", mobile);
                params.put("email", email);
                params.put("name", uname);
                params.put("account_num", accNumber);
                params.put("bank_name", branchName);
                params.put("branch_address", branchAddress);
                params.put("IFSC_code", ifcsCode);
                params.put("pan_card", pancard.getText().toString());

                params.put(KEY_CLIENT_ID, clientId(UpdateProfileDetails.this));

                return params;
            }

        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

    }

    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

    public void saveAccountDetails(String _acNumber,String _bankName,String _branchAddress, String _ifceCode,String pan)
    {
        SharedPreferences acShared= getSharedPreferences("AC_DETAILS",Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = acShared.edit();
        editor.putString("AC_NUMBER",_acNumber);
        editor.putString("BANK_NAME",_bankName);
        editor.putString("BRANCH_ADDRESS",_branchAddress);
        editor.putString("IFSC_CODE",_ifceCode);
        editor.putString("PAN",pan);
        editor.apply();

    }


    public void getGender(){

        myProfilePref = getSharedPreferences("login", Context.MODE_PRIVATE);
        user_Name = myProfilePref.getString("Name", "");
        userId = myProfilePref.getString("User_Id", "");
        gender = myProfilePref.getString("Gender", "");
        webmobileuser = myProfilePref.getString("WebMobileUser", "1");
        mobileNo=myProfilePref.getString(       "Mobileno", "0000000000");



        if (gender.equalsIgnoreCase("Female"))
        {
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                profileimg.setImageDrawable(getApplicationContext().getDrawable(R.drawable.girl_user));
            } else {
                profileimg.setImageResource(R.drawable.girl_user);
                //img_pro_navi.setImageDrawable(getResources().getDrawable(R.drawable.girl_user));
            }
        }else
        {
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                profileimg.setImageDrawable(getApplicationContext().getDrawable(R.drawable.profile));
            } else {

                profileimg.setImageResource(R.drawable.profile);
            }
        }
    }


    public void confirmDialog(final Activity activity) {

        dialog = new Dialog(activity,R.style.floatingDialog);
        dialog.setContentView(R.layout.dialog_confirm);
        TextView proceed =  dialog.findViewById(R.id.txt_proceed);
        TextView recheck =  dialog.findViewById(R.id.txt_recheck);
        recheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        proceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateUserDetails();
                dialog.dismiss();
            }
        });
        dialog.show();

    }



    private void getIfsc(String IFSC){

        Log.e("start","start");
        progressBar.setVisibility(View.VISIBLE);

        final RequestQueue requestQueue = Volley.newRequestQueue(UpdateProfileDetails.this);

        StringRequest stringRequest = new StringRequest(com.android.volley.Request.Method.GET, Configs.urlIfscCode+IFSC, new Response.Listener<String>() {

            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(String response) {
                Log.e("responseIfsc",response);

                try {

                    if (response.contains(txt_UDIFSCCode.getText().toString())){
                        JSONObject jsonObject=new JSONObject(response);
                        txt_UDBranchAddress.setText(jsonObject.getString("ADDRESS"));
                        txt_UDBranchName.setText(jsonObject.getString("BANK"));
                        findViewById(R.id.img_check_done).setVisibility(View.VISIBLE);
                        progressBar.setVisibility(View.GONE);
                    }else {
                        findViewById(R.id.img_check_done).setVisibility(View.GONE);
                        progressBar.setVisibility(View.GONE);
                        txt_UDBranchAddress.setText("");
                        txt_UDBranchName.setText("");
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                    findViewById(R.id.img_check_done).setVisibility(View.GONE);
                    progressBar.setVisibility(View.GONE);
                    txt_UDBranchAddress.setText("");
                    txt_UDBranchName.setText("");
                }

            }
        },

                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        findViewById(R.id.img_check_done).setVisibility(View.GONE);
                        progressBar.setVisibility(View.GONE);
                        txt_UDBranchAddress.setText("");
                        txt_UDBranchName.setText("");
                    }
                });

        requestQueue.add(stringRequest);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


    }


    TextWatcher ifscWathcer = new TextWatcher(){

        @Override
        public void afterTextChanged(Editable arg0) {}

        @Override
        public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {}

        @Override
        public void onTextChanged(CharSequence s, int a, int b, int c) {


            Log.e("ifsc length",String.valueOf(s.length()));
            // output.setText(s);
            if (s.length() == 11) {
                getIfsc(s.toString());
            }
            findViewById(R.id.img_check_done).setVisibility(View.GONE);


        }
    };


}
