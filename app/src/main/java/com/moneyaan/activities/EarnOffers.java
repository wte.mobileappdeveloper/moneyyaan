package com.moneyaan.activities;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import com.moneyaan.R;
import com.moneyaan.configs.Configs;
import com.moneyaan.utils.InternetConnectivity.BaseActivity;
import com.squareup.picasso.Picasso;
import com.startapp.android.publish.adsCommon.StartAppAd;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static com.moneyaan.configs.Configs.KEY_CLIENT_ID;

/////////////////////////////earn offer activity////////////////////////
public class EarnOffers extends BaseActivity{
    private WebView web_instal;
    private TextView txt_Amz,txt_Credit,full_txt,txt_sub_text;
    private String txt_Amz_offers, work_url, url_icon,str_txt_Credit,full_desc,pause;
    private Bundle bundle;
    private Button btn_offers_instal;
    public static final String KEY_UID = "customer_id";
    public static final String KEY_PACKAGEID = "campaign_id";
    public static final String KEY_TASKDURATION= "assing_work_task_duration";
    public String userId, packageId, url_webView;
    private SharedPreferences myPrefsForSesssion;
    Switch languageSwitch;
    StartAppAd startAppAd;
    //private AdView mAdView;

    private ImageView img_icon;
    private Toolbar toolbar;

    private String short_desc,full_desc_hindi,short_desc_hindi,urlWebview_hindi;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_earn_offers);
        toolbar= (Toolbar) findViewById(R.id.toggle_toolbar);
        TextView toolactName= findViewById(R.id.act_name);
        toolactName.setText("Earn Offers");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);



        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();


            }
        });

        startAppAd = new StartAppAd(this);

        myPrefsForSesssion = getSharedPreferences("login", Context.MODE_PRIVATE);
        userId = myPrefsForSesssion.getString("User_Id", "");
        languageSwitch=findViewById(R.id.languageSwitch);
        txt_Amz = (TextView) findViewById(R.id.txt_title_earnOffers);
        txt_Credit= (TextView) findViewById(R.id.txt_Credit);
        txt_sub_text=findViewById(R.id.txt_sub_text);
        full_txt=findViewById(R.id.txt_FullText);
        web_instal = (WebView) findViewById(R.id.web_view_instal);

        btn_offers_instal = (Button) findViewById(R.id.btn_instal_offers);
        btn_offers_instal.setSelected(true);
        btn_offers_instal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                upDateWorkStatusParse();
            }
        });


//        languageSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                if (isChecked){
//
//                }
//            }
//        });


        bundle = getIntent().getExtras();

        txt_Amz_offers = bundle.getString("txt_Amz");
        str_txt_Credit=bundle.getString("txt_Credit");
        url_icon = bundle.getString("icon");
        url_webView = bundle.getString("url_webView");
        work_url = bundle.getString("work_url");
        packageId = bundle.getString("package_id");
        full_desc=bundle.getString("full_desc");
        pause=bundle.getString("pause");
        short_desc=bundle.getString("short_desc");
        full_desc_hindi=bundle.getString("full_desc_hindi");
        short_desc_hindi=bundle.getString("short_desc_hindi");
        urlWebview_hindi=bundle.getString("work_url_hindi");


        try {
            if (!bundle.getString("ClickBLabel").equals("")){
                btn_offers_instal.setText(bundle.getString("ClickBLabel"));
            }
        }catch (NullPointerException e){
            e.printStackTrace();
        }



        img_icon = (ImageView) findViewById(R.id.img_logo);
        txt_Amz.setText(txt_Amz_offers);
        txt_Credit.setText(str_txt_Credit);

        txt_sub_text.setText(full_desc);
        full_txt.setText(short_desc);
        web_instal.loadUrl(url_webView);

        if (url_icon.isEmpty()) { //url.isEmpty()
            Picasso.with(EarnOffers.this)
                    .load(R.drawable.place_holder)
                    .placeholder(R.drawable.place_holder)
                    .error(R.drawable.place_holder)
                    .into(img_icon);

        } else {
            Picasso.with(EarnOffers.this)
                    .load(url_icon)
                    .placeholder(R.drawable.place_holder)
                    .error(R.drawable.place_holder)
                    .into(img_icon); //this is your ImageView
        }



//        //////////////////////////set username value to toolbar
        TextView toolUname=findViewById(R.id.toolbar_uName);
        SharedPreferences sharedpreferences = getSharedPreferences("user_name", Context.MODE_PRIVATE);
        toolUname.setText(sharedpreferences.getString("uName",""));


        findViewById(R.id.sharetask).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shareTask();
            }
        });




        languageSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    txt_sub_text.setText(full_desc_hindi);
                    full_txt.setText(short_desc_hindi);
                    web_instal.loadUrl(urlWebview_hindi);

                }else {
                    txt_sub_text.setText(full_desc);
                    full_txt.setText(short_desc);
                    web_instal.loadUrl(url_webView);

                }
            }
        });


        if (pause.equalsIgnoreCase("Y")){
            btn_offers_instal.setEnabled(false);
            btn_offers_instal.setBackground(getDrawable(R.drawable.button_background_sharp_corner));
            btn_offers_instal.setTextColor(getResources().getColor(R.color.colorPrimary));
            btn_offers_instal.setText(R.string.taskPauseText);
        }

    }



    //////////////api call for work status////////////////////
    private void upDateWorkStatusParse() {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Progressing...");
        progressDialog.show();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        final String taskduration = sdf.format(new Date());
        Log.e("taskduration",taskduration);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Configs.url_updateWorkStatus,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            Log.e("update work resp",response);
                            if (jsonObject.optString("error").equalsIgnoreCase("false")) {
                                Toast.makeText(EarnOffers.this, "Opening in browser", Toast.LENGTH_SHORT).show();
                                Intent inn = new Intent(EarnOffers.this, Home.class);
                                startActivity(inn);

                                //campaignTime(packageId);

                                Intent i = new Intent(Intent.ACTION_VIEW);
                                i.setData(Uri.parse(work_url));
                                startActivity(i);

//                                inProgressParsing();
                            } else if (jsonObject.optString("error").equalsIgnoreCase("true")) {
                                Toast.makeText(EarnOffers.this, "Install Unsuccessful !!", Toast.LENGTH_SHORT).show();
                            }

                            progressDialog.dismiss();

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(EarnOffers.this, "Server is not responding", Toast.LENGTH_SHORT).show();

                        try {
                            progressDialog.dismiss();

                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put(KEY_UID, userId);
                params.put(KEY_PACKAGEID, packageId);
                params.put(KEY_TASKDURATION,taskduration);
                params.put(KEY_CLIENT_ID,Configs.clientId(EarnOffers.this));

                Log.e("update work params",params.toString());
                return params;
            }

        };

        RequestQueue requestQueue = Volley.newRequestQueue(EarnOffers.this);
        requestQueue.add(stringRequest);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
    @Override
    public void onPause() {
        super.onPause();

    }






    private void shareTask(){
        final AlertDialog share;

        final AlertDialog.Builder adb = new AlertDialog.Builder(EarnOffers.this, R.style.floatingDialog);
        View view = EarnOffers.this.getLayoutInflater().inflate(R.layout.dialog_confirm, null);
        adb.setView(view);
        share = adb.create();
        share.getWindow().getAttributes().windowAnimations = R.style.AppTheme;


        TextView txt_title,txt_confirm,txt_cncl;

        txt_confirm=view.findViewById(R.id.txt_proceed);
        txt_cncl=view.findViewById(R.id.txt_recheck);
        txt_title=view.findViewById(R.id.txt_exit);
        txt_title.setText("Share this link with your friend.\nYou will Earn money as your friend complete the task");

        txt_confirm.setText("Share");
        txt_cncl.setText("Cancel");
        txt_cncl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                share.dismiss();
            }
        });

        txt_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
//        String shareBodyText = Configs.sharingLink;
//        sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "Money Yaan");
                sharingIntent.putExtra(Intent.EXTRA_TEXT,  work_url);
                startActivity(Intent.createChooser(sharingIntent, "Sharing Option"));
            }
        });
        share.show();
    }


}
