package com.moneyaan.activities;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.moneyaan.R;
import com.moneyaan.configs.Configs;
import com.moneyaan.utils.InternetConnectivity.BaseActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.moneyaan.configs.Configs.KEY_CLIENT_ID;
import static com.moneyaan.configs.Configs.clientId;

public class VarificationOTP extends BaseActivity {

    Toolbar toolbar;
    EditText edtOtp;
    TextView txtOTPConfirm, txt_timer, txt_resend,otp_sent_number,txt_change_details;
    ImageView img_timer;
    ProgressDialog progressDialog;

    SharedPreferences registSaveDetails;

    boolean rememberMe;
    String uName,uEmail,mobile,password,gender,city,RefferCode,dob;

    int state;
    CountDownTimer countDownTimer;

    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 1;

    public static final String KEY_USER_NAME = "name";
    public static final String KEY_EMAIL = "email";
    public static final String KEY_MOBILE = "mobile";
    public static final String KEY_PASSWORD = "password";
    public static final String KEY_GENDER = "gender";
    public static final String KEY_CITY = "city";
    public static final String KEY_STATE = "state";
    private static final String KEY_REFER_MOBILE="parent_mobile";
    private static final String KEY_DOB="dob";




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verification_otp);

        toolbar = (Toolbar) findViewById(R.id.toggle_toolbar);
        toolbar.setTitle("Verification");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        //Call Registration save Preference

        registSaveDetails = getSharedPreferences("REGIST_DETAILS", Context.MODE_PRIVATE);


        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        edtOtp = findViewById(R.id.edt_otp);
        txtOTPConfirm = findViewById(R.id.txt_otp_confirm);
        txt_timer = findViewById(R.id.txt_timer);
        img_timer = findViewById(R.id.img_timer);
        txt_resend = findViewById(R.id.txt_resend);
        otp_sent_number=findViewById(R.id.otp_sent_number);
        txt_change_details=findViewById(R.id.txt_change_details);
        txt_change_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                getSharedPreferences("login", Context.MODE_PRIVATE).edit().clear().apply();

                startActivity(new Intent(VarificationOTP.this,SignUp.class));
                finish();
            }
        });
//        getRegistationFillDetails();

//        randomNumber();

        progressDialog = new ProgressDialog(this);

        progressDialog.setCancelable(false);
        progressDialog.setMessage("Verification.....");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);

        mobile = registSaveDetails.getString("MOBILE","");
        otp_sent_number.setText(mobile);



        if (rememberMe) {

            Intent i = new Intent(VarificationOTP.this, SignUp.class);
            startActivity(i);
            finish();
        }

        checkAndRequestPermissions();

        // This function for Timer

        countDownTimer = new CountDownTimer(30000, 1000) {

            public void onTick(long millisUntilFinished) {
                txt_timer.setText("Waiting time: " + millisUntilFinished / 1000);

                //   progressDialog.show();
            }

            public void onFinish() {



                txt_timer.setText("Wait or Click on Resend OTP !");
                txt_resend.setVisibility(View.VISIBLE);
                txt_timer.setTextColor(getResources().getColor(R.color.material_green));

                try{
                    img_timer.setBackgroundResource(R.drawable.timer_white);
                }catch (Exception e){
                    img_timer.setImageDrawable(getResources().getDrawable(R.drawable.timer_white));

                }
                //  txtOTPConfirm.setBackgroundResource(R.color.green);
//                progressDialog.show();
            }
        }.start();

        txt_resend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resendOtp();
            }
        });
        txtOTPConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String strOTP = edtOtp.getText().toString();

//.............................enable for our side otp

//                SharedPreferences randmNo = getSharedPreferences("OTP", Context.MODE_PRIVATE);
//
//                String svOTP=randmNo.getString("otp","");
//
//                if (strOTP.equalsIgnoreCase(svOTP)){
//
//                    SharedPreferences saveRefPref=getSharedPreferences("REFER_ID", Context.MODE_PRIVATE);
                confirmOtp(strOTP);


                countDownTimer.onFinish();
                countDownTimer.cancel();

//                }
//                else {
//                    if (strOTP.equalsIgnoreCase("")){
//                        Toast.makeText(VarificationOTP.this,"Please enter OTP",Toast.LENGTH_SHORT).show();
//
//                    }
//                    else {
//                        Toast.makeText(VarificationOTP.this,"OTP does not match",Toast.LENGTH_SHORT).show();
//
//                    }
//                }

            }
        });
        if (checkAndRequestPermissions()) {

        }
        getRegistationFillDetails();

    }






    //    ///////////////////check sms permission///////////////////
    private boolean checkAndRequestPermissions() {
        int permissionSendMessage = ContextCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS);

        int receiveSMS = ContextCompat.checkSelfPermission(this, Manifest.permission.RECEIVE_SMS);

        int readSMS = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_SMS);
        List<String> listPermissionsNeeded = new ArrayList<>();

        if (receiveSMS != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.RECEIVE_MMS);
        }
        if (readSMS != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_SMS);
        }
        if (permissionSendMessage != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.SEND_SMS);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), REQUEST_ID_MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }


    /////////////////////hit confirmotp api///////////////////////
    private void confirmOtp(final String otp) {

        progressDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Configs.otpVerify, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                progressDialog.dismiss();
                Log.e("response regestration",response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    Log.e("Regis Response",response);
                    if (jsonObject.optString("error").toString().equalsIgnoreCase("false")) {


                        Toast.makeText(VarificationOTP.this, "User Created Successfully", Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(VarificationOTP.this,Login.class));
                        finish();
                        getSharedPreferences("login", Context.MODE_PRIVATE).edit().clear().apply();


//                        SharedPreferences.Editor editor=registSaveDetails.edit();
//                        editor.putBoolean("FLAG",false);
//                        editor.clear();
//                        editor.apply();
//
//
//                        Toast.makeText(VarificationOTP.this, "User Created Successfully", Toast.LENGTH_SHORT).show();
//                        bonushDailog();
//                        getSharedPreferences("REFER_ID", Context.MODE_PRIVATE).edit().clear().apply();
////                        alertBonus();
                    } else {
                        Toast.makeText(VarificationOTP.this, jsonObject.optString("error_msg"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {

                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(VarificationOTP.this, "Some error occurred", Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                String userId=getSharedPreferences("login", Context.MODE_PRIVATE).getString("User_Id","");
                params.put("customer_id", userId);
                params.put("otp", otp);
                params.put(KEY_CLIENT_ID, clientId(VarificationOTP.this));



//                params.put(KEY_MOBILE, mobile);
//                params.put(KEY_PASSWORD, password);
//                params.put(KEY_GENDER, gender);
//                params.put(KEY_CITY, city);
//                params.put(KEY_STATE, String.valueOf(state));
//                params.put(KEY_REFER_MOBILE,RefferCode);
//                Log.e("Regis",params.toString());
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

    }


    //------------resend otp----------------
    private void resendOtp() {

        progressDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Configs.url_signup, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                progressDialog.dismiss();
                Log.e("response regestration",response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    Log.e("Regis Response",response);
                    if (jsonObject.optString("error").toString().equalsIgnoreCase("false")) {


                        Toast.makeText(VarificationOTP.this, "OTP sent", Toast.LENGTH_SHORT).show();

                        SharedPreferences myPrefsLogin = getSharedPreferences("login", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = myPrefsLogin.edit();
                        editor.putString("User_Id", jsonObject.getString("customer_id"));
                        editor.putBoolean("FLAG",true);
                        editor.apply();
                    } else {
                        Toast.makeText(VarificationOTP.this, "Mobile or Email already registered", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {

                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(VarificationOTP.this, "Some error occurred", Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
//                Todo add fields missing in API
                params.put(KEY_USER_NAME, uName);
                params.put(KEY_EMAIL, uEmail);
                params.put(KEY_MOBILE, mobile);
                params.put(KEY_PASSWORD, password);
                params.put(KEY_GENDER, gender.substring(0,1));
                params.put(KEY_CITY, city);
                params.put(KEY_STATE, String.valueOf(state));
                params.put(KEY_REFER_MOBILE,RefferCode);
                params.put(KEY_DOB,dob);
                params.put(KEY_CLIENT_ID, clientId(VarificationOTP.this));
                Log.e("Regis",params.toString());

                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

    }

    private void getRegistationFillDetails() {
        uName =registSaveDetails.getString("UNAME","");
        uEmail = registSaveDetails.getString("EMAIL","");
        mobile = registSaveDetails.getString("MOBILE","");
        password = registSaveDetails.getString("PWD","");
        gender = registSaveDetails.getString("GENDER","");
        city = registSaveDetails.getString("CITY","");
        state = registSaveDetails.getInt("STATE",0);
        RefferCode=registSaveDetails.getString("REFFER_CODE","");
        dob=registSaveDetails.getString("DOB","");


//...........Set Sent Mobile number....
        otp_sent_number.setText(mobile);

    }

}