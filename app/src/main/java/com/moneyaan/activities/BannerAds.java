package com.moneyaan.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.CountDownTimer;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.moneyaan.ApiHandler.VolleySingleton;
import com.moneyaan.CommonFunction;
import com.moneyaan.LocalSaved.PreferenceHelper;
import com.moneyaan.R;
import com.moneyaan.configs.Configs;
import com.moneyaan.utils.InternetConnectivity.BaseActivity;
import com.startapp.android.publish.adsCommon.StartAppAd;
import com.startapp.android.publish.adsCommon.StartAppSDK;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import static com.moneyaan.CommonFunction.getCurrentDate;
import static com.moneyaan.configs.Configs.KEY_CLIENT_ID;
import static com.moneyaan.configs.Configs.clientId;
import static com.moneyaan.utils.Constant.ANDROID_ID;
import static com.moneyaan.utils.Constant.BANNER_COUNT_DOWN;
import static com.moneyaan.utils.Constant.BANNER_REWARDSESSION;
import static com.moneyaan.utils.Constant.BANNER_SESSION_DATE;
import static com.moneyaan.utils.Constant.GLOBAR_ADS_UNIT;
import static com.moneyaan.utils.Constant.TODAY_ATTEMPT_BANNER;
import static com.moneyaan.utils.Constant.TODAY_TASK_STATUS_BANNER;
import static com.moneyaan.utils.Constant.USER_CAST;
import static com.moneyaan.utils.Constant.WEB_THE_DIRT_SHEET;



public class BannerAds extends BaseActivity {
    private Toolbar toolbar;
//    private AdView mAdView;
    private ImageView imgRightArrow,img_timer,imgLeftArrow;
    private CountDownTimer countDownTimer;
    private TextView txtCountDown,txtOutOFDown,txtCountCircleDown;
    private RelativeLayout lytCount;
    private WebView webTopNews;
    public static final String KEY_USER_ID = "customer_id";
    int i=0;
    int numberOfAttempt=1;

    String _userId;
    boolean isVideoCompleted=true;
    boolean isCounterFinished=true;
    boolean isReplay=true;
    private ProgressBar webLoading;
    private ProgressDialog progressDialog;
    StartAppAd startAppAd;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_banner_ads);
        toolbar= findViewById(R.id.toggle_toolbar);
        TextView toolactName= findViewById(R.id.act_name);
        toolactName.setText("Banner Ads");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        startAppAd = new StartAppAd(this);
//        mAdView=(AdView) findViewById(R.id.adView);
      ;
        webLoading=findViewById(R.id.webLoading);

        final SharedPreferences preferences = getSharedPreferences("login", Context.MODE_PRIVATE);
        _userId = preferences.getString("User_Id", "");

        //numberOfAdsActive();

        //..................bannerAdLoad();

        imgRightArrow=findViewById(R.id.imgRightArrow);
        imgLeftArrow=findViewById(R.id.imgLeftArrow);
        img_timer=findViewById(R.id.img_timer);
        txtCountDown=findViewById(R.id.txtCountDown);
        txtOutOFDown=findViewById(R.id.txtOutOFDown);
        lytCount=findViewById(R.id.lytCount);
        txtCountDown=findViewById(R.id.txtCountDown);
        txtCountCircleDown=findViewById(R.id.txtCountCircleDown);

        numberOfAttempt=Integer.parseInt(PreferenceHelper.getStringPreference(BannerAds.this,TODAY_ATTEMPT_BANNER));

        i=Integer.parseInt(PreferenceHelper.getStringPreference(BannerAds.this,BANNER_COUNT_DOWN));

        if (PreferenceHelper.getStringPreference(BannerAds.this,TODAY_TASK_STATUS_BANNER).equalsIgnoreCase("false"))
        {

            if (numberOfAttempt<50)
            {
                if (PreferenceHelper.getStringPreference(BannerAds.this,BANNER_COUNT_DOWN).equals("20"))
                {
                    txtOutOFDown.setText("Done");
                    txtCountDown.setText("Today task has been completed !");
                    txtCountDown.setTextColor(getResources().getColor(R.color.material_green));

                }else {

                    txtOutOFDown.setText(PreferenceHelper.getStringPreference(BannerAds.this,BANNER_COUNT_DOWN)+"/"+20);

                }


                if (!PreferenceHelper.isGetFlag(BannerAds.this,BANNER_REWARDSESSION))
                {
                    CommonFunction.reWardDialogPop(BannerAds.this,true,PreferenceHelper.getStringPreference(BannerAds.this,BANNER_COUNT_DOWN)+"/20","Today task is incomplete !");

                }else {

                    CommonFunction.reWardDialogPop(BannerAds.this,true,"Done","Today task has been completed !");
                    imgRightArrow.setVisibility(View.INVISIBLE);
                    imgLeftArrow.setVisibility(View.VISIBLE);
                }

            }else{

                CommonFunction.reWardDialogPop(BannerAds.this,false,PreferenceHelper.getStringPreference(BannerAds.this,TODAY_ATTEMPT_BANNER),"Your all today attempts have finished!");

            }

        }else {

            CommonFunction.reWardDialogPop(BannerAds.this,false,"Done","Today task has been completed !");

        }

        imgRightArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (numberOfAttempt<50)
                {
                    numberOfAttempt++;
                    PreferenceHelper.setStringPreference(BannerAds.this,TODAY_ATTEMPT_BANNER,String.valueOf(numberOfAttempt));

                    if (i<20)
                    {
                        isReplay=false;
                        //ShowAds();
                        startAppAd.showAd();
                        Counter();
                        isVideoCompleted=false;
                    }else {

                        imgRightArrow.setVisibility(View.INVISIBLE);
                    }

                }else {
                    CommonFunction.reWardDialogPop(BannerAds.this,false,PreferenceHelper.getStringPreference(BannerAds.this,TODAY_ATTEMPT_BANNER),"Your all today attempts have finished!");
                }


            }
        });

        imgLeftArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (numberOfAttempt<50)
                {
                    numberOfAttempt++;
                    PreferenceHelper.setStringPreference(BannerAds.this,TODAY_ATTEMPT_BANNER,String.valueOf(numberOfAttempt));

                    //ShowAds();
                    isReplay=true;
                    startAppAd.showAd();
                    Counter();
                    isVideoCompleted=false;

                }else {

                    CommonFunction.reWardDialogPop(BannerAds.this,false,PreferenceHelper.getStringPreference(BannerAds.this,TODAY_ATTEMPT_BANNER),"Your all today attempts have finished.\n Attempt Tomorrow.");

                }


            }
        });

        webTopNews=findViewById(R.id.webTopNews);

        CommonFunction.startWebView(BannerAds.this,webTopNews,WEB_THE_DIRT_SHEET,webLoading);

       // bannerAds(String.valueOf(5));

    }


//    private void numberOfAdsActive()
//    {
//
////        InterstitialAd mInterstitialAd = new InterstitialAd(BannerAds.this);
////        mInterstitialAd.setAdUnitId("ca-app-pub-2539068964968309/4828108518");
////        mInterstitialAd.loadAd(new AdRequest.Builder().build());
////
////
////        InterstitialAd mInterstitialAd2 = new InterstitialAd(BannerAds.this);
////        mInterstitialAd2.setAdUnitId("ca-app-pub-3940256099942544/1033173712");
////        mInterstitialAd2.loadAd(new AdRequest.Builder().build());
////        mInterstitialAdList.add(mInterstitialAd2);
////        mInterstitialAdList.add(mInterstitialAd);
//
//
//        mDatabase.child("intestitial_banner").addValueEventListener(new ValueEventListener() {
//            @Override
//            public void onDataChange(DataSnapshot dataSnapshot) {
//
//                mInterstitialAdList.clear();
//
//                for (DataSnapshot snapshot: dataSnapshot.getChildren())
//                {
//                    if (snapshot.getValue(String.class).equalsIgnoreCase("yes"))
//                    {
//                        InterstitialAd mInterstitialAd = new InterstitialAd(BannerAds.this);
//                        mInterstitialAd.setAdUnitId(GLOBAR_ADS_UNIT + snapshot.getKey());
//                        mInterstitialAd.loadAd(new AdRequest.Builder().build());
//                        mInterstitialAdList.add(mInterstitialAd);
//
//                    }
//
//                }
//            }
//
//            @Override
//            public void onCancelled(DatabaseError databaseError) {
//                // Getting Post failed, log a message
//                Log.e("Tag", "loadPost:onCancelled", databaseError.toException());
//
//            }
//        });
//    }



//    private void ShowAds()
//    {
//
//        if (mInterstitialAdList.size()>0)
//        {
//            final int caseAds=new Random().nextInt(mInterstitialAdList.size());
//
//            if (mInterstitialAdList.get(caseAds).isLoaded())
//            {
//
//                mInterstitialAdList.get(caseAds).show();
//                Counter();
//                mInterstitialAdList.get(caseAds).setAdListener(new AdListener() {
//                    @Override
//                    public void onAdLoaded() {
//
//                        Log.e("TAG","Loaded");
//
//                        // Code to be executed when an ad finishes loading.
//
//                    }
//
//                    @Override
//                    public void onAdFailedToLoad(int errorCode) {
//
//                        if (errorCode==3)
//                        {
//                            Toast.makeText(BannerAds.this, "No Ads available right now!", Toast.LENGTH_SHORT).show();
//                        }else if (errorCode==2)
//                        {
//                            Toast.makeText(BannerAds.this, "Network error right now!", Toast.LENGTH_SHORT).show();
//
//                        }else {
//
//                            Toast.makeText(BannerAds.this, "Internal error right now!", Toast.LENGTH_SHORT).show();
//
//                        }
//                        Log.e("error_video",""+i);
//                    }
//
//                    @Override
//                    public void onAdOpened() {
//                        isVideoCompleted=false;
//                        Toast.makeText(BannerAds.this, "Banner Ad started", Toast.LENGTH_SHORT).show();
//                    }
//
//                    @Override
//                    public void onAdLeftApplication() {
//                        // Code to be executed when the user has left the app.
//                    }
//
//                    @Override
//                    public void onAdClosed() {
//
//                        mInterstitialAdList.get(caseAds).loadAd(new AdRequest.Builder().build());
//                        Toast.makeText(BannerAds.this, "Banner ad has Closed", Toast.LENGTH_SHORT).show();
//
//                        isVideoCompleted=true;
//
//                        if (isVideoCompleted && isCounterFinished) {
//                            txtCountDown.setText("Play next banner ad or replay.");
//                            txtCountDown.setVisibility(View.VISIBLE);
//                            lytCount.setVisibility(View.GONE);
//                            txtCountDown.setTextColor(getResources().getColor(R.color.green_500));
//
//                            if (i<15)
//                            {
//                                imgRightArrow.setVisibility(View.VISIBLE);
//
//                            }else {
//
//                                imgRightArrow.setVisibility(View.INVISIBLE);
//                            }
//
//                            imgLeftArrow.setVisibility(View.VISIBLE);
//
//                        }else {
//                            txtCountDown.setText("Ad banner is not completed. Please play again !");
//                            txtCountDown.setVisibility(View.VISIBLE);
//                            lytCount.setVisibility(View.GONE);
//                            txtCountDown.setTextColor(getResources().getColor(R.color.colorAccent));
//                            imgRightArrow.setVisibility(View.INVISIBLE);
//                            imgLeftArrow.setVisibility(View.VISIBLE);
//                        }
//
//
//                    }
//                });
//            }else {
//                Toast.makeText(this, "Kindly wait for Ads load!", Toast.LENGTH_SHORT).show();
//
//            }
//
//
//        }else {
//
//            Toast.makeText(this, "No Active Ads Right Now !", Toast.LENGTH_SHORT).show();
//        }
//
//    }


    private void Counter()
    {
        // This function for Timer

        countDownTimer = new CountDownTimer(60000, 1000) {

            public void onTick(long millisUntilFinished) {
                txtCountDown.setText("Task is on going: ");
                txtCountDown.setTextColor(getResources().getColor(R.color.yellow_500));
                lytCount.setVisibility(View.VISIBLE);
                txtCountCircleDown.setText(String.valueOf(millisUntilFinished / 1000));
                imgRightArrow.setVisibility(View.INVISIBLE);
                imgLeftArrow.setVisibility(View.VISIBLE);

                //   progressDialog.show();
            }

            public void onFinish() {

                isCounterFinished=true;
                isVideoCompleted=true;

                if (isVideoCompleted && isCounterFinished) {
                    txtCountDown.setText("Play next banner ad or replay.");
                    txtCountDown.setVisibility(View.VISIBLE);
                    lytCount.setVisibility(View.GONE);
                    txtCountDown.setTextColor(getResources().getColor(R.color.green_500));

                    if (i<20)
                    {
                        imgRightArrow.setVisibility(View.VISIBLE);

                    }else {

                        imgRightArrow.setVisibility(View.INVISIBLE);
                    }

                    imgLeftArrow.setVisibility(View.VISIBLE);

                }else {
                    txtCountDown.setText("Ad banner is not completed. Please play again !");
                    txtCountDown.setVisibility(View.VISIBLE);
                    lytCount.setVisibility(View.GONE);
                    txtCountDown.setTextColor(getResources().getColor(R.color.colorAccent));
                    imgRightArrow.setVisibility(View.INVISIBLE);
                    imgLeftArrow.setVisibility(View.VISIBLE);
                }

                if (!isReplay)
                {

                    if (i<19)
                    {
                        i=i+1;
                        txtOutOFDown.setText(i+"/"+20);
                        PreferenceHelper.setStringPreference(BannerAds.this,BANNER_COUNT_DOWN,""+i);
                        PreferenceHelper.setStringPreference(BannerAds.this,BANNER_SESSION_DATE,getCurrentDate());

                    }else {
                        i=i+1;
                        txtOutOFDown.setText("Done");
                        txtCountDown.setText("Today task has been completed !");
                        imgRightArrow.setVisibility(View.INVISIBLE);

                        PreferenceHelper.isFlag(BannerAds.this,BANNER_REWARDSESSION,true);
                        PreferenceHelper.setStringPreference(BannerAds.this,BANNER_COUNT_DOWN,""+i);
                        PreferenceHelper.setStringPreference(BannerAds.this,BANNER_SESSION_DATE,getCurrentDate());
                        CommonFunction.reWardDialogPop(BannerAds.this,true,PreferenceHelper.getStringPreference(BannerAds.this,BANNER_COUNT_DOWN),"Your task has been completed !");

                        bannerAds(String.valueOf(20));

                    }
                }


            }
        }.start();
    }

    private void bannerAdLoad()
    {
//        mAdView.setVisibility(View.VISIBLE);
//        AdRequest adRequest = new AdRequest.Builder().build();
//        mAdView.loadAd(adRequest);

//        mDatabase.child("banner_for_banner/4051570704").addValueEventListener(new ValueEventListener() {
//            @Override
//            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//
//                if (dataSnapshot.getValue(String.class).equalsIgnoreCase("yes"))
//                {
//                    mAdView.setVisibility(View.VISIBLE);
//                    AdRequest adRequest = new AdRequest.Builder().build();
//                    mAdView.loadAd(adRequest);
//
//                }else {
//                    mAdView.setVisibility(View.INVISIBLE);
//                }
//
//            }
//
//            @Override
//            public void onCancelled(@NonNull DatabaseError databaseError) {
//                Log.e("TAG",databaseError.getMessage());
//            }
//        });
    }

    //////////////////reWardAPI /////////////////////////////////////////////
    private void bannerAds(final String userCast)
    {
        progressDialog=new ProgressDialog(this);
        progressDialog.setMessage("Loading...");
        progressDialog.show();
        final String m_androidId = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
        Log.e("TAG",m_androidId);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Configs.url_ADDBANNER, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {


                try {
                    progressDialog.dismiss();
                    Log.e("forgetResponse0",response);
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.optString("error").equalsIgnoreCase("false"))
                    {
                        Toast.makeText(BannerAds.this, "Moon has Uploaded", Toast.LENGTH_SHORT).show();
                    }else
                    {
                        Toast.makeText(BannerAds.this, "Moon has not Uploaded", Toast.LENGTH_SHORT).show();

                    }

                }catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        },new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Toast.makeText(BannerAds.this,"Some error occurred",Toast.LENGTH_SHORT).show();

            }
        }){
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put(KEY_USER_ID, _userId);
                params.put(KEY_CLIENT_ID, clientId(BannerAds.this));
                params.put(USER_CAST, userCast);
                params.put(ANDROID_ID,m_androidId);

                return params;
            }

        };

        VolleySingleton.getInstance(BannerAds.this).addToRequestQueue(stringRequest);

    }

    @Override
    public void onBackPressed() {

        super.onBackPressed();
    }

}
