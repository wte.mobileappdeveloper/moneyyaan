package com.moneyaan.activities;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.moneyaan.LocalSaved.PreferenceHelper;
import com.moneyaan.R;
import com.moneyaan.configs.Configs;
import com.startapp.android.publish.adsCommon.StartAppAd;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import static com.moneyaan.CommonFunction.getCurrentDate;
import static com.moneyaan.configs.Configs.KEY_CLIENT_ID;
import static com.moneyaan.configs.Configs.clientId;
import static com.moneyaan.utils.Constant.BANNER_COUNT_DOWN;
import static com.moneyaan.utils.Constant.BANNER_REWARDSESSION;
import static com.moneyaan.utils.Constant.BANNER_SESSION_DATE;
import static com.moneyaan.utils.Constant.COUNT_DOWN;
import static com.moneyaan.utils.Constant.REWARDSESSION;
import static com.moneyaan.utils.Constant.SESSION_DATE;
import static com.moneyaan.utils.Constant.SPECL_COUNT_DOWN;
import static com.moneyaan.utils.Constant.SPEC_REWARDSESSION;
import static com.moneyaan.utils.Constant.SPEC_SESSION_DATE;
import static com.moneyaan.utils.Constant.TODAY_ATTEMPT_BANNER;
import static com.moneyaan.utils.Constant.TODAY_ATTEMPT_SPEC;

public class SplashScreen extends AppCompatActivity {

    String parMobile="0000000000";
    RequestQueue requestQueue;
    StartAppAd   startAppAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_FULLSCREEN| View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);

        setContentView(R.layout.activity_splash_screen);

        Log.e("SECU",getCurrentDate()+" "+PreferenceHelper.getStringPreference(SplashScreen.this,SPEC_SESSION_DATE));

        startAppAd = new StartAppAd(this);


        //.....................For Reward Video.......................

        if (!PreferenceHelper.getStringPreference(SplashScreen.this,SESSION_DATE).equalsIgnoreCase(getCurrentDate()))
        {
            PreferenceHelper.removeToKey(SplashScreen.this,REWARDSESSION);
            PreferenceHelper.removeToKey(SplashScreen.this,COUNT_DOWN);
            PreferenceHelper.removeToKey(SplashScreen.this,SESSION_DATE);
        }

       //...........For Special Video Secu ..........................

        if (!PreferenceHelper.getStringPreference(SplashScreen.this,SPEC_SESSION_DATE).equalsIgnoreCase(getCurrentDate()))
        {
            PreferenceHelper.removeToKey(SplashScreen.this,SPEC_REWARDSESSION);
            PreferenceHelper.removeToKey(SplashScreen.this,SPECL_COUNT_DOWN);
            PreferenceHelper.removeToKey(SplashScreen.this,SPEC_SESSION_DATE);
            PreferenceHelper.removeToKey(SplashScreen.this,TODAY_ATTEMPT_SPEC);
            PreferenceHelper.isFlag(SplashScreen.this,SPEC_REWARDSESSION,false);

        }

        //................For Banner Video.........................

        if (!PreferenceHelper.getStringPreference(SplashScreen.this,BANNER_SESSION_DATE).equalsIgnoreCase(getCurrentDate()))
        {
            PreferenceHelper.removeToKey(SplashScreen.this,BANNER_REWARDSESSION);
            PreferenceHelper.removeToKey(SplashScreen.this,BANNER_COUNT_DOWN);
            PreferenceHelper.removeToKey(SplashScreen.this,BANNER_SESSION_DATE);
            PreferenceHelper.removeToKey(SplashScreen.this,TODAY_ATTEMPT_BANNER);
            PreferenceHelper.isFlag(SplashScreen.this,BANNER_REWARDSESSION,false);


        }

        getLiveAppVersion();


    }

    public void getLiveAppVersion(){

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Configs.url_versionChecker,
                new Response.Listener<String>() {
                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                    @Override
                    public void onResponse(String response) {

                        Log.e("version splash resp",response);
                        try {
                            JSONObject jsonObject=new JSONObject(response);

                            if (jsonObject.getString("error").equalsIgnoreCase("true")){
                                appCheckerVersion(0,"N");

                            }else if(jsonObject.getString("error").equalsIgnoreCase("false")){

                                int liveVersionCode=Integer.parseInt(jsonObject.getString("version_code"));
                                String mandatoryUpdate=jsonObject.getString("mandatory");

                                appCheckerVersion(liveVersionCode,mandatoryUpdate);



                            }

                        }catch (Exception e){

                            e.printStackTrace();
                        }



                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                getLiveAppVersion();

            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put(KEY_CLIENT_ID, clientId(SplashScreen.this));
                return params;
            }

        };


//        --------------this will prevent memory out problem--------------
        if (requestQueue == null) {
            requestQueue = Volley.newRequestQueue(SplashScreen.this);
        }
        requestQueue.add(stringRequest);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

    }
    //---------------- For App Check Function-------------------
    private void appCheckerVersion(int liveVersionCode,String mandatoryUpdate) {
        try {


            PackageInfo packageInfo = this.getPackageManager().getPackageInfo(getPackageName(), 0);
            int version_code = packageInfo.versionCode;


            if (liveVersionCode>version_code ) {

                showDialogAddRoute(mandatoryUpdate);

            }else {
                Handler handler=new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        Intent intent=new Intent(SplashScreen.this,WelcomeActivity.class);
                        startActivity(intent);
                        finish();

                    }
                }, 5000);

            }


        }

        catch (Exception e) {

//            ----in case of any error proceed to app-------------
            e.getStackTrace();
            Handler handler=new Handler();handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    Intent intent=new Intent(SplashScreen.this,WelcomeActivity.class);
                    startActivity(intent);
                    finish();

                }
            }, 5000);
        }
    }
    //-----------update app dialog-----------------------------
    public void showDialogAddRoute(String mandatory_update){
        final Dialog dialog;


        dialog = new Dialog(this);

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_update);

        Button update=dialog.findViewById(R.id.btnUpdate);
        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent=new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("https://play.google.com/store/apps/details?id=com.moneyaan"));
                startActivity(intent);

            }
        });
        // Set Event For Cancel pop

        Button btnCancel=dialog.findViewById(R.id.btnNoThanks);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intent intent=new Intent(SplashScreen.this,WelcomeActivity.class);
                startActivity(intent);
                finish();
            }
        });


        if (mandatory_update.equalsIgnoreCase("Y")){
            btnCancel.setVisibility(View.GONE);
        }else {
            btnCancel.setVisibility(View.VISIBLE);

        }

        dialog.show();
    }



    @Override
    protected void onStart() {
        super.onStart();

    }


}
