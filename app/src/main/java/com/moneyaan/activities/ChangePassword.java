package com.moneyaan.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.moneyaan.R;
import com.moneyaan.configs.Configs;
import com.moneyaan.utils.InternetConnectivity.BaseActivity;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.moneyaan.configs.Configs.KEY_CLIENT_ID;
import static com.moneyaan.configs.Configs.clientId;

public class ChangePassword extends BaseActivity {

    Button change;
    private static final String KEY_CURRENT_PASS = "password";
    private static final String KEY_NEW_PASS = "newpassword";
    public static final String KEY_MOBILE = "mobileno";
    public static final String KEY_CUSTMRID = "customer_id";

    private Toolbar toolbar;
    ProgressDialog progressDoalog;
    EditText current_pass,new_pass,cnfrm_pass;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);

        toolbar= findViewById(R.id.toggle_toolbar);
        TextView toolactName= findViewById(R.id.act_name);
        toolactName.setText("Change Password");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {onBackPressed();
            }
        });

        change=findViewById(R.id.btn_changePass);
        current_pass=findViewById(R.id.edt__current_pass);
        new_pass=findViewById(R.id.edt__new_pass);
        cnfrm_pass=findViewById(R.id.edt__cnfrm_pass);

        change.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validation();
            }
        });


    }


    private  void validation(){
        if (current_pass.getText().toString().equalsIgnoreCase("")) {
            current_pass.setError("Please Enter Current Password");
            current_pass.requestFocus();
        }else if (new_pass.getText().toString().equalsIgnoreCase("")) {
            new_pass.setError("Please Enter New Password");
            new_pass.requestFocus();
        }else if (cnfrm_pass.getText().toString().equalsIgnoreCase("")) {
            cnfrm_pass.setError("Please Enter New Password");
            cnfrm_pass.requestFocus();
        }else if (!cnfrm_pass.getText().toString().equals(new_pass.getText().toString())) {
            cnfrm_pass.setError("Password Does Not Match");
            cnfrm_pass.requestFocus();
        }
        else {

            changePassword();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition( R.anim.ritht_to_left_in, R.anim.ritht_to_left );

    }
    private void changePassword()
    {
        final String  currentPass= current_pass.getText().toString();
        final String  newPass= cnfrm_pass.getText().toString();
        progressDoalog = new ProgressDialog(this);
        progressDoalog.setMessage("Please wait...");
        progressDoalog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Configs.url_changePassword, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                progressDoalog.dismiss();

                try {
                    Log.e("Response0",response);
                    JSONObject jsonObject = new JSONObject(response);

                    if (jsonObject.getString("error").equalsIgnoreCase("false")){
                        Toast.makeText(ChangePassword.this,"Password changed successfully",Toast.LENGTH_SHORT).show();

                        startActivity(new Intent(ChangePassword.this,Home.class));

                    }else if (jsonObject.getString("error").equalsIgnoreCase("true")){
                        Toast.makeText(ChangePassword.this,"Please enter correct current password",Toast.LENGTH_SHORT).show();

                    }


                }catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        },new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDoalog.dismiss();
                Toast.makeText(ChangePassword.this,"Some error occurred",Toast.LENGTH_SHORT).show();

            }
        }){
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                SharedPreferences myPrefsLogin = getSharedPreferences("login", Context.MODE_PRIVATE);

                String mobile=myPrefsLogin.getString("Mobileno","");
                params.put(KEY_CURRENT_PASS, currentPass);
                params.put(KEY_NEW_PASS, newPass);
                params.put(KEY_MOBILE, mobile);
                params.put(KEY_CUSTMRID,myPrefsLogin.getString("User_Id",""));
                params.put(KEY_CLIENT_ID, clientId(ChangePassword.this));

                Log.e("params",params.toString());

                return params;
            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


    }
    @Override
    public void onPause() {
        super.onPause();
    }
}
